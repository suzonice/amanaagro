	<div id="content">
		<div id="content_middle">
			<div id="products">
				<?php
				$category_id=$sub_category_id=NULL;
				
				if(isset($_GET['catid']))
				{
					$category_id=$_GET['catid'];
				}
				
				if(isset($_GET['scatid']))
				{
					$sub_category_id=$_GET['scatid'];
				}
				
				$category_products = get_category_products($category_id, $sub_category_id);
				if(sizeof($category_products)>0)
				{
					foreach($category_products as $prod)
					{
						$sell_price = $prod->product_price - $prod->discount_amount;
						
						if(!empty($prod->discount_amount))
						{
							$price_html = '<h3 class="price" style="display:inline;">BDT '.$sell_price.' <span style="color:#E21225;text-decoration:line-through;margin-left:5px;">BDT '.$prod->product_price.'</span>
							</h3>';
						}
						else
						{
							$price_html = '<h3 class="price">BDT '.$prod->product_price.'</h3>';
						}

						echo '<div class="product_bg gap" id="p_pic">
							<a href="'.base_url().'product-view/?pid='.$prod->product_id.'&pname='.$prod->product_title.'" target="_blank"><img src="'.get_media_path($prod->media_id).'" alt="'.$prod->product_title.'"/></a>
							<div class="featured_pro_name">
								<h3 class="pro_name">'.$prod->product_title.'</h3>
							</div>
							'.$price_html.'
							<div id="button">
								<div class="pro_order" style="cursor:pointer;"><a href="'.base_url().'order/?pid='.$prod->product_id.'" target="_blank">Order Now</a></div>
								<a class="pro_view_a" href="'.base_url().'product-view/?pid='.$prod->product_id.'&pname='.$prod->product_title.'" target="_blank">
									<div class="pro_view" style="padding-top:8px;"><span style="padding-top:15px;">View Details</span></div>
								</a>
							</div>
							<div class="clear"></div>
						</div>';
					}
				}
				?>
			</div>
		</div>
		
		
		
	</div>