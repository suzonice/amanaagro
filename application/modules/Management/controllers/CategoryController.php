<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}

	}

	public function index()
	{
		$data['main'] = "Category";
		$data['active'] = "Category view";
		$data['categorys'] = $this->MainModel->getAllData('', 'category', '*', 'category_id DESC');
		$data['pageContent'] = $this->load->view('management/Category/category_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "Category registration form ";
		$data['main'] = "Category";
		$data['active'] = "Add Category";
		$data['pageContent'] = $this->load->view('management/Category/category_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store()
	{


		$data['category_name'] = $this->input->post('category_name');
		$this->form_validation->set_rules('category_name', 'Category name','required' );

		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('category', $data);
			if ($result) {
				$this->session->set_flashdata('message', "Category added successfully !!!!");
				redirect('category-list');
			}
		} else {
			redirect('category-list');
		}


	}

    public function show($id)
    {
        $data['member'] = $this->MainModel->getSingleData('category_id', $id, 'category', '*');
//print_r($data);exit();
        $memberId = $data['member']->category_id;
        if (isset($memberId)) {
            $data['title'] = "Category profile page";
            $data['main'] = "Category";
            $data['active'] = "pofile of member";
            $data['pageContent'] = $this->load->view('management/Members/members_show', $data, true);
            $this->load->view('layouts/main', $data);
        } else {
            $this->session->set_flashdata('message', "The element you are trying to edit does not exist.");
            redirect('member-list');
        }
    }

	public function edit($id)
	{

		$data['Category'] = $this->MainModel->getSingleData('category_id', $id, 'category', '*');
		$memberId = $data['Category']->category_id;
		if (isset($memberId)) {
			$data['title'] = "Category update page ";
			$data['main'] = "Category";
			$data['active'] = "Update Category";
			$data['pageContent'] = $this->load->view('management/Category/category_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('category-list');
		}

	}

	public function update()
	{
		$memberId = $this->input->post('category_id');
		// check if the element exists before trying to edit it
		$memberData = $this->MainModel->getSingleData('category_id', $memberId, 'category', '*');
		$memberId = $memberData->category_id;

		if (isset($memberId)) {
            $data['category_name'] = $this->input->post('category_name');

            $this->form_validation->set_rules('category_name', 'Category name','required' );

			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('category_id', $memberId, 'category', $data);
				if ($result) {
					$this->session->set_flashdata('message', "Category updated successfully !!!!");
					redirect('category-list');
				}
			} else {

               $this->edit();
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('member-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('category_id', $id, 'category', '*');
		$memberId = $memberData->category_id;
		if (isset($memberId)) {
			$result = $this->MainModel->deleteData('category_id', $memberId, 'category');
			if ($result) {
				$this->session->set_flashdata('message', "Category deleted successfully !!!!");
				redirect('category-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('category-list');
		}
	}
}
