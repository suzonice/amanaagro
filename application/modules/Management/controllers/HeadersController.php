<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HeadersController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->library('form_validation');
		$this->load->library('image_lib');
		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}
	}



	public function create()
	{
		$data['title'] = "header Update form ";
		$data['main'] = "header";
		$data['active'] = "Update header";
        $data['header'] = $this->MainModel->getSingleData('header_id', '1', 'headers', '*');

        //var_dump($data['header']);exit();

        $data['pageContent'] = $this->load->view('management/headers/headers_create', $data, true);
		$this->load->view('layouts/main', $data);
	}



	public function update()
	{


			$old_logo=$this->input->post('old_logo');
			$data['logo']=$this->input->post('old_logo');
			if(isset($_FILES["logo"]["name"]))
			{
				if((($_FILES["logo"]["type"]=="image/jpg") || ($_FILES["logo"]["type"]=="image/jpeg") || ($_FILES["logo"]["type"]=="image/png") || ($_FILES["logo"]["type"]=="image/gif"))){
					if(!empty($old_logo)){
						unlink($old_logo);

					}
					$uploaded_image_path = "uploads/sliders/".time().'-'.$_FILES["logo"]["name"];
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$this->load->library('upload', $config);
					if ($_FILES["logo"]["error"] > 0) {
						echo "Return Code: " . $_FILES["logo"]["error"] . "<br />";
					}
					else
					{
						move_uploaded_file ($_FILES["logo"]["tmp_name"],$uploaded_image_path);
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = FALSE;
						$config['quality'] = '60%';
						$config['width'] = 500;
						$config['height'] = 390;
						$config['new_image'] = $uploaded_image_path;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$data['logo']=$uploaded_image_path;

					}
				}
			}
		$data['header_one'] = $this->input->post('header_one');
		$data['header_location'] = $this->input->post('header_location');
		$data['header_two'] = $this->input->post('header_two');
		$data['header_mobile'] = $this->input->post('header_mobile');
		$data['header_email'] = $this->input->post('header_email');

		$this->form_validation->set_rules('header_one', 'headers name', 'required');
			$this->form_validation->set_rules('header_two', 'headers name', 'required');
			$this->form_validation->set_rules('header_mobile', 'headers name', 'required');
			$this->form_validation->set_rules('header_email', 'headers name', 'required');
			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('header_id', 1, 'headers', $data);
				if ($result) {
					$this->session->set_flashdata('message', "headers updated successfully !!!!");
					redirect('header-create');
				}
			} else {

                $this->session->set_flashdata('message', "value reqiured");
                redirect('header-create');
			}


	}

	public function destroy($id)
	{
		$headersData = $this->MainModel->getSingleData('headers_id', $id, 'headers', '*');
		$headersId = $headersData->headers_id;
		if (isset($headersId)) {
			$result = $this->MainModel->deleteData('headers_id', $headersId, 'headers');
			if ($result) {
				$this->session->set_flashdata('message', "headers deleted successfully !!!!");
				redirect('headers-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('headers-list');
		}
	}
}
