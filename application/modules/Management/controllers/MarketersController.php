<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MarketersController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$this->load->library('image_lib');

		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}


	}

	public function index()
	{
		$data['main'] = "Marketing Officer";
		$data['active'] = "View marketing officer";
		$query="select * from marketer left
join districts on districts.district_id=marketer.district_id
";
		$data['marketers']  = $this->MainModel->AllQueryDalta($query);

//		$data['marketers'] = $this->MainModel->getAllData('', 'marketer', '*', 'marketer_id DESC');
		$data['pageContent'] = $this->load->view('management/marketers/marketers_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "Marketing Officer registration form ";
		$data['main'] = "Marketing Officer";
		$data['active'] = "Add Marketing officer";
		$data['districts'] = $this->MainModel->getAllData('', 'districts', '*', 'district_id DESC');

		$data['pageContent'] = $this->load->view('management/marketers/marketers_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store ()
	{



		if (isset($_FILES["marketer_picture_path"]["name"])) {
			if ((($_FILES["marketer_picture_path"]["type"] == "image/jpg") || ($_FILES["marketer_picture_path"]["type"] == "image/jpeg") || ($_FILES["marketer_picture_path"]["type"] == "image/png") || ($_FILES["marketer_picture_path"]["type"] == "image/gif"))) {

				if ($_FILES["marketer_picture_path"]["error"] > 0) {
					echo "Return Code: " . $_FILES["marketer_picture_path"]["error"] . "<br />";
				} else {
					$uploaded_image_path = "uploads/marketers/" . time() . '-' . $_FILES["marketer_picture_path"]["name"];
					$uploaded_file_path = "uploads/marketers/" . $_FILES["marketer_picture_path"]["name"];

					if (!file_exists($uploaded_file_path)) {
						move_uploaded_file($_FILES["marketer_picture_path"]["tmp_name"], $uploaded_image_path);
						// resize image
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['new_image'] = $uploaded_image_path;
						$config['quality'] = '40%';
						$config['width'] = 300;
						$config['height'] = 300;
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						$data['marketer_picture_path'] = $config['source_image'];
					}
				}
			}
		}

		$data['marketer_name'] = $this->input->post('marketer_name');
		$data['marketer_mobile'] = $this->input->post('marketer_mobile');
		$data['marketer_email'] = $this->input->post('marketer_email');
		$data['marketer_address'] = $this->input->post('marketer_address');
		$data['district_id'] = $this->input->post('district_id');

		$this->form_validation->set_rules('district_id', 'Marketing officer name','required' );
		$this->form_validation->set_rules('marketer_address', 'member title ','required' );
		$this->form_validation->set_rules('marketer_name', 'Marketing officer phone','required' );
		$this->form_validation->set_rules('marketer_mobile', 'Marketing officer phone','required' );
		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('marketer', $data);
			if ($result) {
				$this->session->set_flashdata('message', "Marketing officer added successfully !!!!");
				redirect('marketer-list');
			}
		} else {
			redirect('marketer-create');
		}


	}

    public function show($id)
    {
        $data['member'] = $this->MainModel->getSingleData('marketer_id', $id, 'marketers', '*');
//print_r($data);exit();
        $memberId = $data['member']->marketer_id;
        if (isset($memberId)) {
            $data['title'] = "Marketing officer profile page";
            $data['main'] = "Marketing officer";
            $data['active'] = "pofile of member";
            $data['pageContent'] = $this->load->view('management/marketers/marketers_show', $data, true);
            $this->load->view('layouts/main', $data);
        } else {
            $this->session->set_flashdata('message', "The element you are trying to edit does not exist.");
            redirect('member-list');
        }
    }

	public function edit($id)
	{
		$data['districts'] = $this->MainModel->getAllData('', 'districts', '*', 'district_id DESC');
		$data['marketer'] = $this->MainModel->getSingleData('marketer_id', $id, 'marketer', '*');
		$memberId = $data['marketer']->marketer_id;
		if (isset($memberId)) {
			$data['title'] = "Marketing officer update page ";
			$data['main'] = "Marketing officer";
			$data['active'] = "Update Marketing officer";
			$data['pageContent'] = $this->load->view('management/marketers/marketers_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('member-list');
		}

	}

	public function update()
	{
		$memberId = $this->input->post('marketer_id');
		// check if the element exists before trying to edit it
		$memberData = $this->MainModel->getSingleData('marketer_id', $memberId, 'marketer', '*');
		$memberId = $memberData->marketer_id;

		if (isset($memberId)) {


			$old_student_picture_path=$this->input->post('old_marketer_picture_path');
			$data['marketer_picture_path']=$this->input->post('old_marketer_picture_path');
			if(isset($_FILES["marketer_picture_path"]["name"]))
			{
				if((($_FILES["marketer_picture_path"]["type"]=="image/jpg") || ($_FILES["marketer_picture_path"]["type"]=="image/jpeg") || ($_FILES["marketer_picture_path"]["type"]=="image/png") || ($_FILES["marketer_picture_path"]["type"]=="image/gif"))){
					if(!empty($old_student_picture_path)){
						unlink($old_student_picture_path);

					}
					$uploaded_image_path = "uploads/marketers/".time().'-'.$_FILES["marketer_picture_path"]["name"];
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$this->load->library('upload', $config);
					if ($_FILES["marketer_picture_path"]["error"] > 0) {
						echo "Return Code: " . $_FILES["marketer_picture_path"]["error"] . "<br />";
					}
					else
					{
						move_uploaded_file ($_FILES["marketer_picture_path"]["tmp_name"],$uploaded_image_path);

						//$picture = $this->upload->data();

						//var_dump($picture);exit();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = False;
						$config['quality'] = '40%';
						$config['width'] = 300;
						$config['height'] = 300;
						$config['new_image'] = $uploaded_image_path;
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						$data['marketer_picture_path'] = $config['source_image'];

					}
				}
			}

			$data['marketer_name'] = $this->input->post('marketer_name');
			$data['marketer_mobile'] = $this->input->post('marketer_mobile');
			$data['marketer_email'] = $this->input->post('marketer_email');
			$data['marketer_address'] = $this->input->post('marketer_address');
			$data['district_id'] = $this->input->post('district_id');

			$this->form_validation->set_rules('district_id', 'Marketing officer name','required' );
			$this->form_validation->set_rules('marketer_address', 'member title ','required' );
			$this->form_validation->set_rules('marketer_name', 'Marketing officer phone','required' );
			$this->form_validation->set_rules('marketer_mobile', 'Marketing officer phone','required' );
			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('marketer_id', $memberId, 'marketer', $data);
				if ($result) {
					$this->session->set_flashdata('message', "Marketing officer updated successfully !!!!");
					redirect('marketer-list');
				}
			} else {

               $this->edit();
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('marketer-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('marketer_id', $id, 'marketer', '*');
		$memberId = $memberData->marketer_id;
		if (isset($memberId)) {
			$result = $this->MainModel->deleteData('marketer_id', $memberId, 'marketer');
			if ($result) {
				$this->session->set_flashdata('message', "Marketing officer deleted successfully !!!!");
				redirect('marketer-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('marketer-list');
		}
	}
}
