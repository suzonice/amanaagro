<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SatingsController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->library('form_validation');
		$this->load->library('image_lib');
		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}
	}



	public function create()
	{
		$data['title'] = "Page Update form ";
		$data['main'] = "Page setting";
		$data['active'] = "Update page setting";
        $data['sating'] = $this->MainModel->getSingleData('satting_id', '1', 'setting', '*');
        $data['pageContent'] = $this->load->view('management/sating/sating_create', $data, true);
		$this->load->view('layouts/main', $data);
	}



	public function update()
	{


			$satting_id=$this->input->post('satting_id');
			$data['satting_about_company']=$this->input->post('satting_about_company');
			$data['satting_about_product']=$this->input->post('satting_about_product');
			$data['satting_about_satatics']=$this->input->post('satting_about_satatics');
			if(isset($satting_id)){
				$result = $this->MainModel->updateData('satting_id', $satting_id, 'setting', $data);
				if ($result) {
					$this->session->set_flashdata('message', "Page setting updated successfully !!!!");
					redirect('setting-create');
				}
			}

	}

	public function destroy($id)
	{
		$headersData = $this->MainModel->getSingleData('headers_id', $id, 'headers', '*');
		$headersId = $headersData->headers_id;
		if (isset($headersId)) {
			$result = $this->MainModel->deleteData('headers_id', $headersId, 'headers');
			if ($result) {
				$this->session->set_flashdata('message', "headers deleted successfully !!!!");
				redirect('headers-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('headers-list');
		}
	}
}
