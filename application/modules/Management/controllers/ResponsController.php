<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResponsController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$this->load->library('image_lib');
		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}


	}

	public function index()
	{
		$data['main'] = "Responser respons";
		$data['active'] = "Responser respons view";
		$data['Responsers'] = $this->MainModel->getAllData('', 'respons', '*', 'respons_id DESC');
		$data['pageContent'] = $this->load->view('management/respons/respons_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "Responser registration form ";
		$data['main'] = "Responser";
		$data['active'] = "Add Responser";
		$data['pageContent'] = $this->load->view('management/respons/respons_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store()
	{


		if (isset($_FILES["respons_image_path"]["name"])) {
			if ((($_FILES["respons_image_path"]["type"] == "image/jpg") || ($_FILES["respons_image_path"]["type"] == "image/jpeg") || ($_FILES["respons_image_path"]["type"] == "image/png") || ($_FILES["respons_image_path"]["type"] == "image/gif"))) {

				if ($_FILES["respons_image_path"]["error"] > 0) {
					echo "Return Code: " . $_FILES["respons_image_path"]["error"] . "<br />";
				} else {
					$uploaded_image_path = "./uploads/" . time() . '-' . $_FILES["respons_image_path"]["name"];
					$uploaded_file_path = "uploads/" . $_FILES["respons_image_path"]["name"];

					if (!file_exists($uploaded_file_path)) {
						move_uploaded_file($_FILES["respons_image_path"]["tmp_name"], $uploaded_image_path);
						// resize image
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['new_image'] = $uploaded_image_path;
						$config['quality'] = '60%';
						$config['width'] = 800;
						$config['height'] = 500;
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						$data['respons_image_path'] = $config['source_image'];
					}
				}
			}
		}
		$data['respons_name'] = $this->input->post('respons_name');
		$this->form_validation->set_rules('respons_name', 'Responser name','required' );

		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('respons', $data);
			if ($result) {
				$this->session->set_flashdata('message', "Responser added successfully !!!!");
				redirect('respons-list');
			}
		} else {
			redirect('respons-create');
		}


	}



	public function edit($id)
	{

		$data['Responser'] = $this->MainModel->getSingleData('respons_id', $id, 'respons', '*');
		$ResponserId = $data['Responser']->respons_id;
		if (isset($ResponserId)) {
			$data['title'] = "Responser update page ";
			$data['main'] = "Responser";
			$data['active'] = "Update Responser";
			$data['pageContent'] = $this->load->view('management/respons/respons_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('respons-list');
		}

	}

	public function update()
	{
		$student_id = $this->input->post('respons_id');
		$Student = $this->MainModel->getSingleData('respons_id', $student_id, 'respons', '*');
		$studentId = $Student->respons_id;


		if (isset($studentId)) {

			$old_student_picture_path=$this->input->post('old_respons_image_path');
			$data['respons_image_path']=$this->input->post('old_respons_image_path');
			if(isset($_FILES["respons_image_path"]["name"]))
			{
				if((($_FILES["respons_image_path"]["type"]=="image/jpg") || ($_FILES["respons_image_path"]["type"]=="image/jpeg") || ($_FILES["respons_image_path"]["type"]=="image/png") || ($_FILES["respons_image_path"]["type"]=="image/gif"))){
					if(!empty($old_student_picture_path)){
						unlink($old_student_picture_path);

					}
					$uploaded_image_path = "uploads/".time().'-'.$_FILES["respons_image_path"]["name"];
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$this->load->library('upload', $config);
					if ($_FILES["respons_image_path"]["error"] > 0) {
						echo "Return Code: " . $_FILES["respons_image_path"]["error"] . "<br />";
					}
					else
					{
						move_uploaded_file ($_FILES["respons_image_path"]["tmp_name"],$uploaded_image_path);

						//$picture = $this->upload->data();

						//var_dump($picture);exit();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = FALSE;
						$config['quality'] = '60%';
						$config['width'] = 800;
						$config['height'] = 500;
						$config['new_image'] = $uploaded_image_path;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$data['respons_image_path']=$uploaded_image_path;

					}
				}
			}


			$data['respons_name'] = $this->input->post('respons_name');
			$this->form_validation->set_rules('respons_name', 'Responser name','required' );
			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('respons_id', $student_id, 'respons', $data);
				if ($result) {
					$this->session->set_flashdata('message', "Responser updated successfully !!!!");
					redirect('respons-list');
				}
			} else {

				$this->session->set_flashdata('message', "value reqiured");
				redirect('respons-update');
			}
		}
		else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('respons-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('respons_id', $id, 'respons', '*');
		$ResponserId = $memberData->respons_id;
		if (isset($ResponserId)) {
			$result = $this->MainModel->deleteData('respons_id', $ResponserId, 'respons');
			if ($result) {
				$this->session->set_flashdata('message', "Responser deleted successfully !!!!");
				redirect('respons-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('respons-list');
		}
	}
}
