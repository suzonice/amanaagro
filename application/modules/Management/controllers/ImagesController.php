<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImagesController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$this->load->library('image_lib');
		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}


	}

	public function index()
	{
		$data['main'] = "Gallery images";
		$data['active'] = "Gallery images view";
		$data['galerys'] = $this->MainModel->getAllData('', 'galery', '*', 'galery_id DESC');
		$data['pageContent'] = $this->load->view('management/images/images_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "galery registration form ";
		$data['main'] = "galery";
		$data['active'] = "Add galery";
		$data['pageContent'] = $this->load->view('management/images/images_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store()
	{


		if (isset($_FILES["galery_image_path"]["name"])) {
			if ((($_FILES["galery_image_path"]["type"] == "image/jpg") || ($_FILES["galery_image_path"]["type"] == "image/jpeg") || ($_FILES["galery_image_path"]["type"] == "image/png") || ($_FILES["galery_image_path"]["type"] == "image/gif"))) {

				if ($_FILES["galery_image_path"]["error"] > 0) {
					echo "Return Code: " . $_FILES["galery_image_path"]["error"] . "<br />";
				} else {
					$uploaded_image_path = "./uploads/galerys/" . time() . '-' . $_FILES["galery_image_path"]["name"];
					$uploaded_file_path = "uploads/galerys/" . $_FILES["galery_image_path"]["name"];

					if (!file_exists($uploaded_file_path)) {
						move_uploaded_file($_FILES["galery_image_path"]["tmp_name"], $uploaded_image_path);
						// resize image
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['new_image'] = $uploaded_image_path;
						$config['quality'] = '60%';
						$config['width'] = 800;
						$config['height'] = 500;
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						$data['galery_image_path'] = $config['source_image'];
					}
				}
			}
		}
		$data['galery_name'] = $this->input->post('galery_name');
		$this->form_validation->set_rules('galery_name', 'galery name','required' );

		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('galery', $data);
			if ($result) {
				$this->session->set_flashdata('message', "galery added successfully !!!!");
				redirect('galery-list');
			}
		} else {
			redirect('galery-create');
		}


	}

    public function show($id)
    {
        $data['member'] = $this->MainModel->getSingleData('galery_id', $id, 'galery', '*');
//print_r($data);exit();
        $galeryId = $data['member']->galery_id;
        if (isset($galeryId)) {
            $data['title'] = "galery profile page";
            $data['main'] = "galery";
            $data['active'] = "pofile of member";
            $data['pageContent'] = $this->load->view('management/Members/members_show', $data, true);
            $this->load->view('layouts/main', $data);
        } else {
            $this->session->set_flashdata('message', "The element you are trying to edit does not exist.");
            redirect('member-list');
        }
    }

	public function edit($id)
	{

		$data['galery'] = $this->MainModel->getSingleData('galery_id', $id, 'galery', '*');
		$galeryId = $data['galery']->galery_id;
		if (isset($galeryId)) {
			$data['title'] = "galery update page ";
			$data['main'] = "galery";
			$data['active'] = "Update galery";
			$data['pageContent'] = $this->load->view('management/images/images_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('galery-list');
		}

	}

	public function update()
	{
		$student_id = $this->input->post('galery_id');
		$Student = $this->MainModel->getSingleData('galery_id', $student_id, 'galery', '*');
		$studentId = $Student->galery_id;


		if (isset($studentId)) {

			$old_student_picture_path=$this->input->post('old_galery_image_path');
			$data['galery_image_path']=$this->input->post('old_galery_image_path');
			if(isset($_FILES["galery_image_path"]["name"]))
			{
				if((($_FILES["galery_image_path"]["type"]=="image/jpg") || ($_FILES["galery_image_path"]["type"]=="image/jpeg") || ($_FILES["galery_image_path"]["type"]=="image/png") || ($_FILES["galery_image_path"]["type"]=="image/gif"))){
					if(!empty($old_student_picture_path)){
						unlink($old_student_picture_path);

					}
					$uploaded_image_path = "uploads/galerys/".time().'-'.$_FILES["galery_image_path"]["name"];
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$this->load->library('upload', $config);
					if ($_FILES["galery_image_path"]["error"] > 0) {
						echo "Return Code: " . $_FILES["galery_image_path"]["error"] . "<br />";
					}
					else
					{
						move_uploaded_file ($_FILES["galery_image_path"]["tmp_name"],$uploaded_image_path);

						//$picture = $this->upload->data();

						//var_dump($picture);exit();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = FALSE;
						$config['quality'] = '60%';
						$config['width'] = 800;
						$config['height'] = 500;
						$config['new_image'] = $uploaded_image_path;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$data['galery_image_path']=$uploaded_image_path;

					}
				}
			}


			$data['galery_name'] = $this->input->post('galery_name');
			$this->form_validation->set_rules('galery_name', 'galery name','required' );
			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('galery_id', $student_id, 'galery', $data);
				if ($result) {
					$this->session->set_flashdata('message', "galery updated successfully !!!!");
					redirect('galery-list');
				}
			} else {

				$this->session->set_flashdata('message', "value reqiured");
				redirect('galery-update');
			}
		}
		else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('galery-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('galery_id', $id, 'galery', '*');
		$galeryId = $memberData->galery_id;
		if (isset($galeryId)) {
			$result = $this->MainModel->deleteData('galery_id', $galeryId, 'galery');
			if ($result) {
				$this->session->set_flashdata('message', "galery deleted successfully !!!!");
				redirect('galery-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('galery-list');
		}
	}
}
