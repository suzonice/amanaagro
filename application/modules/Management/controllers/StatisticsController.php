<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatisticsController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}

	}

	public function index()
	{
		$data['main'] = "Statistics";
		$data['active'] = "Statistics view";
		$data['categorys'] = $this->MainModel->getAllData('', 'statistics', '*', 'statistic_id DESC');
		$data['pageContent'] = $this->load->view('management/Statistics/category_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "Statistics registration form ";
		$data['main'] = "Statistics";
		$data['active'] = "Add Statistics";
		$data['statistic'] = $this->MainModel->getSingleData('statistic_id', 1, 'statistics', '*');

		$data['pageContent'] = $this->load->view('management/statistic/statistic_create', $data, true);
		$this->load->view('layouts/main', $data);

	}


	public function store()
	{
		$data['category_name'] = $this->input->post('category_name');
		$this->form_validation->set_rules('category_name', 'Statistics name','required' );

		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('statistics', $data);
			if ($result) {
				$this->session->set_flashdata('message', "Statistics added successfully !!!!");
				redirect('statistics-list');
			}
		} else {
			redirect('statistics-list');
		}
	}

    public function show($id)
    {
        $data['member'] = $this->MainModel->getSingleData('statistic_id', $id, 'statistics', '*');
        $memberId = $data['member']->statistic_id;
        if (isset($memberId)) {
            $data['title'] = "Statistics profile page";
            $data['main'] = "Statistics";
            $data['active'] = "pofile of member";
            $data['pageContent'] = $this->load->view('management/Members/members_show', $data, true);
            $this->load->view('layouts/main', $data);
        } else {
            $this->session->set_flashdata('message', "The element you are trying to edit does not exist.");
            redirect('member-list');
        }
    }

	public function edit($id)
	{
		$data['Statistics'] = $this->MainModel->getSingleData('statistic_id', $id, 'statistics', '*');
		$memberId = $data['Statistics']->statistic_id;
		if (isset($memberId)) {
			$data['title'] = "Statistics update page ";
			$data['main'] = "Statistics";
			$data['active'] = "Update Statistics";
			$data['pageContent'] = $this->load->view('management/Statistics/category_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('statistics-list');
		}
	}

	public function update()
	{
		$memberId = $this->input->post('statistic_id');
		$memberData = $this->MainModel->getSingleData('statistic_id', $memberId, 'statistics', '*');
		$memberId = $memberData->statistic_id;
		if (isset($memberId)) {
            $data['statistic_branch'] = $this->input->post('statistic_branch');
            $data['statistic_stuff'] = $this->input->post('statistic_stuff');
            $data['statistic_client'] = $this->input->post('statistic_client');
            $data['statistic_product'] = $this->input->post('statistic_product');
            $this->form_validation->set_rules('statistic_product', 'Statistics name','required' );
            $this->form_validation->set_rules('statistic_client', 'Statistics name','required' );
            $this->form_validation->set_rules('statistic_stuff', 'Statistics name','required' );
            $this->form_validation->set_rules('statistic_branch', 'Statistics name','required' );
			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('statistic_id', $memberId, 'statistics', $data);
				if ($result) {
					$this->session->set_flashdata('message', "Statistics updated successfully !!!!");
					redirect('statistic-create');
				}
			} else {
               $this->edit();
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('statistic-create');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('statistic_id', $id, 'statistics', '*');
		$memberId = $memberData->statistic_id;
		if (isset($memberId)) {
			$result = $this->MainModel->deleteData('statistic_id', $memberId, 'statistics');
			if ($result) {
				$this->session->set_flashdata('message', "Statistics deleted successfully !!!!");
				redirect('statistics-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('statistics-list');
		}
	}
}
