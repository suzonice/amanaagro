<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsController extends MX_Controller {

	public function __construct()
	{
		$this->load->model('MainModel');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('image_lib');

		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}


	}

	public function index()
	{
		$data['title'] = "products List ";
		$data['main'] = "Product";
		$data['active'] = "Product view";
		$data['products'] = $this->MainModel->getAllData('', 'products', '*', 'product_id DESC');
		$data['category'] = $this->MainModel->getAllData('', 'category', '*', 'category_id DESC');

		$data['pageContent'] = $this->load->view('management/products/products_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "Product registration form ";
		$data['main'] = "Products";
		$data['active'] = "Add product";
		$data['category'] = $this->MainModel->getAllData('', 'category', '*', 'category_id DESC');


		$data['pageContent'] = $this->load->view('management/Products/products_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store()
	{


		if (isset($_FILES["product_images"]["name"])) {
			if ((($_FILES["product_images"]["type"] == "image/jpg") || ($_FILES["product_images"]["type"] == "image/jpeg") || ($_FILES["product_images"]["type"] == "image/png") || ($_FILES["product_images"]["type"] == "image/gif"))) {

				if ($_FILES["product_images"]["error"] > 0) {
					echo "Return Code: " . $_FILES["product_images"]["error"] . "<br />";
				} else {
					$uploaded_image_path = "uploads/products/" . time() . '-' . $_FILES["product_images"]["name"];
					$uploaded_file_path = "uploads/products/" . $_FILES["product_images"]["name"];

					if (!file_exists($uploaded_file_path)) {
						move_uploaded_file($_FILES["product_images"]["tmp_name"], $uploaded_image_path);
						// resize image
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['new_image'] = $uploaded_image_path;
						$config['quality'] = '60%';
						$config['width'] = 800;
						$config['height'] = 500;
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						$data['product_images'] = $config['source_image'];
					}
				}
			}
		}

		$data['product_name'] = $this->input->post('product_name');
		$data['product_poriman'] = $this->input->post('product_poriman');
		$data['product_work'] = $this->input->post('product_work');
		$data['product_kill'] = $this->input->post('product_kill');
		$data['product_element'] = $this->input->post('product_element');
		$data['product_akor'] = $this->input->post('product_akor');
		$data['product_liter'] = $this->input->post('product_liter');
		$data['category_id'] = $this->input->post('category_id');

		$this->form_validation->set_rules('product_name', 'Student name', 'required');
	
		if ($this->form_validation->run()) {
			$result = $this->MainModel->insertData('products', $data);
			if ($result) {
				$this->session->set_flashdata('message', "product added successfully !!!!");
				redirect('product-list');
			}
		} else {

			$this->session->set_flashdata('error', "All field need to fillUp");
			redirect('product-create');
		}


	}

	public function show($id)

	{
		echo 'ff';
		$data['student'] = $this->MainModel->getSingleData('product_id', $id, 'products', '*');
		$product_id = $data['student']->product_id;
		if (isset($product_id)) {
			$data['title'] = "product profile page";
			$data['main'] = "product";
			$data['active'] = "pofile of product";
			$data['pageContent'] = $this->load->view('management/products/products_show', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('product-list');
		}
	}

	public function edit($id)
	{
		$data['product']= $this->MainModel->getSingleData('product_id', $id, 'products', '*');
		$data['category'] = $this->MainModel->getAllData('', 'category', '*', 'category_id DESC');

		$product_id= $data['product']->product_id;
		if (isset($product_id)) {
			$data['title'] = "Student update page ";
			$data['main'] = "Student";
			$data['second'] = "Student option";
			$data['active'] = "update Student";
			$data['pageContent'] = $this->load->view('management/products/products_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('student-list');
		}

	}

	public function update()
	{
		$product_id = $this->input->post('product_id');
		// check if the element exists before trying to edit it
		$Student = $this->MainModel->getSingleData('product_id', $product_id, 'products', '*');
		$product_id = $Student->product_id;

		if (isset($product_id)) {


			$old_student_picture_path=$this->input->post('old_product_images');
			$data['product_images']=$this->input->post('old_product_images');
			if(isset($_FILES["product_images"]["name"]))
			{
				if((($_FILES["product_images"]["type"]=="image/jpg") || ($_FILES["product_images"]["type"]=="image/jpeg") || ($_FILES["product_images"]["type"]=="image/png") || ($_FILES["product_images"]["type"]=="image/gif"))){
					if(!empty($old_student_picture_path)){
						unlink($old_student_picture_path);

					}
					$uploaded_image_path = "uploads/products/".time().'-'.$_FILES["product_images"]["name"];
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$this->load->library('upload', $config);
					if ($_FILES["product_images"]["error"] > 0) {
						echo "Return Code: " . $_FILES["product_images"]["error"] . "<br />";
					}
					else
					{
						move_uploaded_file ($_FILES["product_images"]["tmp_name"],$uploaded_image_path);

						//$picture = $this->upload->data();

						//var_dump($picture);exit();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = FALSE;
						$config['quality'] = '60%';
						$config['width'] = 800;
						$config['height'] = 500;
						$config['new_image'] = $uploaded_image_path;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$data['product_images']=$uploaded_image_path;

					}
				}
			}
			$data['product_name'] = $this->input->post('product_name');
			$data['product_poriman'] = $this->input->post('product_poriman');
			$data['product_work'] = $this->input->post('product_work');
			$data['product_kill'] = $this->input->post('product_kill');
			$data['product_element'] = $this->input->post('product_element');
			$data['product_akor'] = $this->input->post('product_akor');
			$data['product_liter'] = $this->input->post('product_liter');
			$data['category_id'] = $this->input->post('category_id');


			$this->form_validation->set_rules('product_name', 'Student name', 'required');
			$this->form_validation->set_rules('category_id', 'Student name', 'required');

			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('product_id', $product_id, 'products', $data);
				if ($result) {
					$this->session->set_flashdata('message', "product updated successfully !!!!");
					redirect('product-list');
				}
			} else {

				$this->session->set_flashdata('message', "value reqiured");
				redirect('product-update');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('product-list');
		}

	}

	public function destroy($id)
{
	$Student= $this->MainModel->getSingleData('product_id', $id, 'products', '*');
	$product_id = $Student->product_id;
	if (isset($product_id)) {
		$result = $this->MainModel->deleteData('product_id', $id, 'products');
		if ($result) {
			$this->session->set_flashdata('message', "Product deleted successfully !!!!");
			redirect('product-list');
		}
	} else {
		$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
		redirect('product-list');
	}
}

public  function  user(){
	$data['title'] = "User registation form ";
	$data['main'] = "User";
	$data['active'] = "Add User";
	$data['marketers'] = $this->MainModel->getAllData('', 'members', '*', 'member_id DESC');
	$data['pageContent'] = $this->load->view('management/products/user_create', $data, true);
	$this->load->view('layouts/main', $data);

}
}
