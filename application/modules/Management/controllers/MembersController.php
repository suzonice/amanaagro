<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MembersController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$this->load->library('image_lib');

		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}


	}

	public function index()
	{
		$data['main'] = "Member";
		$data['active'] = "Member view";
		$data['members'] = $this->MainModel->getAllData('', 'members', '*', 'member_id DESC');
		$data['pageContent'] = $this->load->view('management/Members/members_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "Member registration form ";
		$data['main'] = "Member";
		$data['active'] = "Add Member";
		$data['pageContent'] = $this->load->view('management/Members/members_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store()
	{


		if (isset($_FILES["member_picture_path"]["name"])) {
			if ((($_FILES["member_picture_path"]["type"] == "image/jpg") || ($_FILES["member_picture_path"]["type"] == "image/jpeg") || ($_FILES["member_picture_path"]["type"] == "image/png") || ($_FILES["member_picture_path"]["type"] == "image/gif"))) {

				if ($_FILES["member_picture_path"]["error"] > 0) {
					echo "Return Code: " . $_FILES["member_picture_path"]["error"] . "<br />";
				} else {
					$uploaded_image_path = "uploads/members/" . time() . '-' . $_FILES["member_picture_path"]["name"];
					$uploaded_file_path = "uploads/members/" . $_FILES["member_picture_path"]["name"];

					if (!file_exists($uploaded_file_path)) {
						move_uploaded_file($_FILES["member_picture_path"]["tmp_name"], $uploaded_image_path);
						// resize image
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['new_image'] = $uploaded_image_path;
						$config['quality'] = '70%';
						$config['width'] = 500;
						$config['height'] = 500;
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						$data['member_picture_path'] = $config['source_image'];
					}
				}
			}
		}

		$data['member_name'] = $this->input->post('member_name');
		$data['email_id'] = $this->input->post('email_id');
		$data['member_title'] = $this->input->post('member_title');
		$data['member_phone'] = $this->input->post('member_phone');
		$data['facebook_id'] = $this->input->post('facebook_id');
		$data['linkined_id'] = $this->input->post('linkined_id');
		$data['twiter_id'] = $this->input->post('twiter_id');
		$this->form_validation->set_rules('member_name', 'Member name','required' );
		$this->form_validation->set_rules('member_title', 'member title ','required' );
		$this->form_validation->set_rules('member_phone', 'Member phone','required' );
		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('members', $data);
			if ($result) {
				$this->session->set_flashdata('message', "Member added successfully !!!!");
				redirect('member-list');
			}
		} else {
			$this->create();
		}


	}

    public function show($id)
    {
        $data['member'] = $this->MainModel->getSingleData('member_id', $id, 'members', '*');
//print_r($data);exit();
        $memberId = $data['member']->member_id;
        if (isset($memberId)) {
            $data['title'] = "Member profile page";
            $data['main'] = "Member";
            $data['active'] = "pofile of member";
            $data['pageContent'] = $this->load->view('management/Members/members_show', $data, true);
            $this->load->view('layouts/main', $data);
        } else {
            $this->session->set_flashdata('message', "The element you are trying to edit does not exist.");
            redirect('member-list');
        }
    }

	public function edit($id)
	{

		$data['member'] = $this->MainModel->getSingleData('member_id', $id, 'members', '*');
		$memberId = $data['member']->member_id;
		if (isset($memberId)) {
			$data['title'] = "Member update page ";
			$data['main'] = "Member";
			$data['active'] = "Update Member";
			$data['pageContent'] = $this->load->view('management/Members/members_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('member-list');
		}

	}

	public function update()
	{
		$memberId = $this->input->post('member_id');
		// check if the element exists before trying to edit it
		$memberData = $this->MainModel->getSingleData('member_id', $memberId, 'members', '*');
		$memberId = $memberData->member_id;

		if (isset($memberId)) {


			$old_student_picture_path=$this->input->post('old_member_picture_path');
			$data['member_picture_path']=$this->input->post('old_member_picture_path');
			if(isset($_FILES["member_picture_path"]["name"]))
			{
				if((($_FILES["member_picture_path"]["type"]=="image/jpg") || ($_FILES["member_picture_path"]["type"]=="image/jpeg") || ($_FILES["member_picture_path"]["type"]=="image/png") || ($_FILES["member_picture_path"]["type"]=="image/gif"))){
					if(!empty($old_student_picture_path)){
						unlink($old_student_picture_path);

					}
					$uploaded_image_path = "uploads/sliders/".time().'-'.$_FILES["member_picture_path"]["name"];
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$this->load->library('upload', $config);
					if ($_FILES["member_picture_path"]["error"] > 0) {
						echo "Return Code: " . $_FILES["member_picture_path"]["error"] . "<br />";
					}
					else
					{
						move_uploaded_file ($_FILES["member_picture_path"]["tmp_name"],$uploaded_image_path);

						//$picture = $this->upload->data();

						//var_dump($picture);exit();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = FALSE;
						$config['quality'] = '60%';
						$config['width'] = 1000;
						$config['height'] = 500;
						$config['new_image'] = $uploaded_image_path;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$data['member_picture_path']=$uploaded_image_path;

					}
				}
			}
			
            $data['member_name'] = $this->input->post('member_name');
            $data['email_id'] = $this->input->post('email_id');
            $data['member_title'] = $this->input->post('member_title');
            $data['member_phone'] = $this->input->post('member_phone');
            $data['facebook_id'] = $this->input->post('facebook_id');
            $data['linkined_id'] = $this->input->post('linkined_id');
            $data['twiter_id'] = $this->input->post('twiter_id');
            $this->form_validation->set_rules('member_name', 'Member name','required' );
            $this->form_validation->set_rules('member_title', 'member title ','required' );
            $this->form_validation->set_rules('member_phone', 'Member phone','required' );
			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('member_id', $memberId, 'members', $data);
				if ($result) {
					$this->session->set_flashdata('message', "Member updated successfully !!!!");
					redirect('member-list');
				}
			} else {

               $this->edit();
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('member-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('member_id', $id, 'members', '*');
		$memberId = $memberData->member_id;
		if (isset($memberId)) {
			$result = $this->MainModel->deleteData('member_id', $memberId, 'members');
			if ($result) {
				$this->session->set_flashdata('message', "Member deleted successfully !!!!");
				redirect('member-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('member-list');
		}
	}
}
