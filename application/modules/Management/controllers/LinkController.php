<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LinkController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}

	}

	public function index()
	{
		$data['main'] = "link";
		$data['active'] = "link view";
		$data['links'] = $this->MainModel->getAllData('', 'link', '*', 'link_id DESC');
		$data['pageContent'] = $this->load->view('management/link/link_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "Link registration form ";
		$data['main'] = "link";
		$data['active'] = "Add link";
		$data['pageContent'] = $this->load->view('management/link/link_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store()
	{


		$data['link_title'] = $this->input->post('link_title');
		$data['link'] = $this->input->post('link');
		$this->form_validation->set_rules('link_title', 'link name','required' );
		$this->form_validation->set_rules('link', 'link name','required' );

		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('link', $data);
			if ($result) {
				$this->session->set_flashdata('message', "link added successfully !!!!");
				redirect('link-list');
			}
		} else {
			redirect('link-list');
		}


	}

    public function show($id)
    {
        $data['member'] = $this->MainModel->getSingleData('link_id', $id, 'link', '*');
//print_r($data);exit();
        $memberId = $data['member']->link_id;
        if (isset($memberId)) {
            $data['title'] = "link profile page";
            $data['main'] = "link";
            $data['active'] = "pofile of member";
            $data['pageContent'] = $this->load->view('management/Members/members_show', $data, true);
            $this->load->view('layouts/main', $data);
        } else {
            $this->session->set_flashdata('message', "The element you are trying to edit does not exist.");
            redirect('member-list');
        }
    }

	public function edit($id)
	{

		$data['link'] = $this->MainModel->getSingleData('link_id', $id, 'link', '*');
		$memberId = $data['link']->link_id;
		if (isset($memberId)) {
			$data['title'] = "link update page ";
			$data['main'] = "link";
			$data['active'] = "Update link";
			$data['pageContent'] = $this->load->view('management/link/link_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('link-list');
		}

	}

	public function update()
	{
		$memberId = $this->input->post('link_id');

		$memberData = $this->MainModel->getSingleData('link_id', $memberId, 'link', '*');
		$memberId = $memberData->link_id;

		if (isset($memberId)) {
			$data['link_title'] = $this->input->post('link_title');
			$data['link'] = $this->input->post('link');

            $this->form_validation->set_rules('link_title', 'link name','required' );
            $this->form_validation->set_rules('link', 'link name','required' );

			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('link_id', $memberId, 'link', $data);
				if ($result) {
					$this->session->set_flashdata('message', "link updated successfully !!!!");
					redirect('link-list');
				}
			} else {

               $this->edit();
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('member-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('link_id', $id, 'link', '*');
		$memberId = $memberData->link_id;
		if (isset($memberId)) {
			$result = $this->MainModel->deleteData('link_id', $memberId, 'link');
			if ($result) {
				$this->session->set_flashdata('message', "link deleted successfully !!!!");
				redirect('link-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('link-list');
		}
	}
}
