<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SlidersController extends MX_Controller
{

	public function __construct()
	{
		$this->load->model('MainModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$this->load->library('image_lib');
		$userRole=$this->session->userdata('user_role');
		if($userRole ==null){
			redirect('admin');
		}


	}

	public function index()
	{
		$data['main'] = "slider";
		$data['active'] = "slider view";
		$data['sliders'] = $this->MainModel->getAllData('', 'slider', '*', 'slider_id DESC');
		$data['pageContent'] = $this->load->view('management/slider/slider_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{
		$data['title'] = "slider registration form ";
		$data['main'] = "slider";
		$data['active'] = "Add slider";
		$data['pageContent'] = $this->load->view('management/slider/slider_create', $data, true);
		$this->load->view('layouts/main', $data);
	}


	public function store()
	{


		if (isset($_FILES["slider_image"]["name"])) {
			if ((($_FILES["slider_image"]["type"] == "image/jpg") || ($_FILES["slider_image"]["type"] == "image/jpeg") || ($_FILES["slider_image"]["type"] == "image/png") || ($_FILES["slider_image"]["type"] == "image/gif"))) {

				if ($_FILES["slider_image"]["error"] > 0) {
					echo "Return Code: " . $_FILES["slider_image"]["error"] . "<br />";
				} else {
					$uploaded_image_path = "./uploads/sliders/" . time() . '-' . $_FILES["slider_image"]["name"];
					$uploaded_file_path = "uploads/sliders/" . $_FILES["slider_image"]["name"];

					if (!file_exists($uploaded_file_path)) {
						move_uploaded_file($_FILES["slider_image"]["tmp_name"], $uploaded_image_path);
						// resize image
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['new_image'] = $uploaded_image_path;
						$config['quality'] = '60%';
						$config['width'] = 1920;
						$config['height'] = 500;
						$this->image_lib->clear();
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						$data['slider_image'] = $config['source_image'];
					}
				}
			}
		}
		$data['slider_heading'] = $this->input->post('slider_heading');
		$this->form_validation->set_rules('slider_heading', 'slider name','required' );

		if ($this->form_validation->run() ==true) {
			$result = $this->MainModel->insertData('slider', $data);
			if ($result) {
				$this->session->set_flashdata('message', "slider added successfully !!!!");
				redirect('slider-list');
			}
		} else {
			redirect('slider-create');
		}


	}

    public function show($id)
    {
        $data['member'] = $this->MainModel->getSingleData('slider_id', $id, 'slider', '*');
//print_r($data);exit();
        $sliderId = $data['member']->slider_id;
        if (isset($sliderId)) {
            $data['title'] = "slider profile page";
            $data['main'] = "slider";
            $data['active'] = "pofile of member";
            $data['pageContent'] = $this->load->view('management/Members/members_show', $data, true);
            $this->load->view('layouts/main', $data);
        } else {
            $this->session->set_flashdata('message', "The element you are trying to edit does not exist.");
            redirect('member-list');
        }
    }

	public function edit($id)
	{

		$data['slider'] = $this->MainModel->getSingleData('slider_id', $id, 'slider', '*');
		$sliderId = $data['slider']->slider_id;
		if (isset($sliderId)) {
			$data['title'] = "slider update page ";
			$data['main'] = "slider";
			$data['active'] = "Update slider";
			$data['pageContent'] = $this->load->view('management/slider/slider_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('slider-list');
		}

	}

	public function update()
	{
		$student_id = $this->input->post('slider_id');
		$Student = $this->MainModel->getSingleData('slider_id', $student_id, 'slider', '*');
		$studentId = $Student->slider_id;


		if (isset($studentId)) {

			$old_student_picture_path=$this->input->post('old_slider_image');
			$data['slider_image']=$this->input->post('old_slider_image');
			if(isset($_FILES["slider_image"]["name"]))
			{
				if((($_FILES["slider_image"]["type"]=="image/jpg") || ($_FILES["slider_image"]["type"]=="image/jpeg") || ($_FILES["slider_image"]["type"]=="image/png") || ($_FILES["slider_image"]["type"]=="image/gif"))){
					if(!empty($old_student_picture_path)){
						unlink($old_student_picture_path);

					}
					$uploaded_image_path = "uploads/sliders/".time().'-'.$_FILES["slider_image"]["name"];
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$this->load->library('upload', $config);
					if ($_FILES["slider_image"]["error"] > 0) {
						echo "Return Code: " . $_FILES["slider_image"]["error"] . "<br />";
					}
					else
					{
						move_uploaded_file ($_FILES["slider_image"]["tmp_name"],$uploaded_image_path);

						//$picture = $this->upload->data();

						//var_dump($picture);exit();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $uploaded_image_path;
						$config['create_thumb'] = false;
						$config['maintain_ratio'] = FALSE;
						$config['quality'] = '60%';
						$config['width'] = 1920;
						$config['height'] = 500;
						$config['new_image'] = $uploaded_image_path;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$data['slider_image']=$uploaded_image_path;

					}
				}
			}
			//$data['slider_image'] = $this->input->post('old_slider_image');


			$data['slider_heading'] = $this->input->post('slider_heading');
			$this->form_validation->set_rules('slider_heading', 'slider name','required' );
			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('slider_id', $student_id, 'slider', $data);
				if ($result) {
					$this->session->set_flashdata('message', "slider updated successfully !!!!");
					redirect('slider-list');
				}
			} else {

				$this->session->set_flashdata('message', "value reqiured");
				redirect('slider-update');
			}
		}
		else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('slider-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('slider_id', $id, 'slider', '*');
		$sliderId = $memberData->slider_id;
		if (isset($sliderId)) {
			$result = $this->MainModel->deleteData('slider_id', $sliderId, 'slider');
			if ($result) {
				$this->session->set_flashdata('message', "slider deleted successfully !!!!");
				redirect('slider-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('slider-list');
		}
	}
}
