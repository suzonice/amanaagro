<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Member Name<span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($member)) echo $member->member_name; ?>"
               class="form-control" name="member_name" placeholder="Provide member name">
        <span id="spanId" style="color:red"> <?php echo form_error('member_name');  ?></span>

        <input type="hidden" id="field-1" name="member_id"
               value="<?php if (isset($member)) echo $member->member_id; ?>" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Member designation <span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($member)) echo $member->member_title; ?>"
               class="form-control" name="member_title" placeholder="Provide member title">
        <span id="spanId" style="color:red"> <?php echo form_error('member_title');  ?></span>


    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Member Phone number<span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($member)) echo $member->member_phone; ?>"
               class="form-control" name="member_phone" placeholder="Provide member phone">

        <span id="spanId" style="color:red"> <?php echo form_error('member_phone');  ?></span>
    </div>
</div>
<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Member email <span style="color:red"></span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($member)) echo $member->email_id; ?>"
               class="form-control" name="email_id" placeholder="Provide member email">

    </div>
</div>


<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Member facebook Id <span style="color:red"></span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($member)) echo $member->facebook_id; ?>"
               class="form-control" name="facebook_id" placeholder="Provide member facebookid">


    </div>
</div>


<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Member  twiter Id <span style="color:red"></span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($member)) echo $member->twiter_id; ?>"
               class="form-control" name="twiter_id" placeholder="Provide member twiter id">


    </div>
</div>


<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Member  linked Id <span style="color:red"></span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($member)) echo $member->linkined_id; ?>"
               class="form-control" name="linkined_id" placeholder="Provide linked id ">


    </div>
</div>


<?php if(!empty($member->member_picture_path)):?>
	<div class="form-group">
		<label for="field-1" class="col-sm-3 control-label">Member picture</label>
		<div class="col-sm-8">
			<img width="70" height="50" src="<?php echo base_url(); if(isset($member)){ echo $member->member_picture_path;} ?>"/>

			<input type="hidden"  class="form-control" name="old_member_picture_path" value="<?php  if(isset($member)){ echo $member->member_picture_path;} ?>">
		</div>
	</div>
<?php endif;?>
<div class="form-group">
	<label for="field-1" class="col-sm-3 control-label">Member picture</label>
	<div class="col-sm-8">
		<input type="file"  class="form-control" name="member_picture_path">
	</div>
</div>






					
