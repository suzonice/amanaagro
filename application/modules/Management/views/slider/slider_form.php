<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Slider Name<span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($slider)) echo $slider->slider_heading; ?>"
               class="form-control" name="slider_heading" placeholder="Provide Slider name">

        <input type="hidden" id="field-1" name="slider_id"
               value="<?php if (isset($slider)) echo $slider->slider_id; ?>" class="form-control">
    </div>
</div>
<?php if(!empty($slider->slider_image)):?>
	<div class="form-group">
		<label for="field-1" class="col-sm-3 control-label">Slider picture</label>
		<div class="col-sm-8">
			<img width="70" height="50" src="<?php echo base_url(); if(isset($slider)){ echo $slider->slider_image;} ?>"/>

			<input type="hidden"  class="form-control" name="old_slider_image" value="<?php  if(isset($slider)){ echo $slider->slider_image;} ?>">
		</div>
	</div>
<?php endif;?>
<div class="form-group">
	<label for="field-1" class="col-sm-3 control-label">Slider picture</label>
	<div class="col-sm-8">
		<input type="file"  class="form-control" name="slider_image">
	</div>
</div>
