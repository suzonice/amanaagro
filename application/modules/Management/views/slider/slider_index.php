
<div class="col-md-offset-1 col-md-10">
<div class="box  box-success">
	<div class="box-header with-border">
		<h3 class="box-title"><a class="btn btn-success" href="<?php echo base_url();?>slider-create"><i class="fa fa-plus-circle"></i>Add new</span></a></h3>


	</div>
	<div class="box-body">

		<table id="example1" class="table table-bordered table-striped">
			<thead>
			<tr>
				<th>Serial</th>
				<th>SliderName</th>
				<th>Picture</th>

				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			<?php if (isset($sliders)):

				$count = 1;
				//var_dump($count);exit();
				foreach ($sliders as $slider):

					?>
					<tr>
						<td><?php echo $count; ?></td>
						<td><?php echo $slider->slider_heading; ?></td>
						<td><?php
							if(!empty($slider->slider_image)):
								?>
								<img width="70" height="50" src="<?php echo base_url();echo $slider->slider_image; ?>"/>
							<?php
							else:
								?>
								<img width="70" height="50"  src="<?php echo base_url() ?>uploads/teachers/teacher.png"/>
							<?php endif;
							?></td>


<td>
							<a href="<?php echo base_url() ?>slider-edit/<?php echo $slider->slider_id; ?>"
							<span class="glyphicon glyphicon-edit btn btn-success"></span>
							</a>
							<a href="<?php echo base_url() ?>slider-delete/<?php echo $slider->slider_id; ?>"
							   onclick="return confirm('Are you want to delete this information :press Ok for delete otherwise Cancel')">
								<span class="glyphicon glyphicon-trash btn btn-danger"></span>
							</a>


						</td>

					</tr>

					<?php
					$count--;
				endforeach;
			endif; ?>

			</tbody>

		</table>


	</div>

</div>
</div>
