
<div class="col-md-offset-2 col-md-8">

<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $title; ?></h3>


	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-1">
			</div>
			<div class="col-md-7">
				<table class="table ">
					<tbody>
					<tr>
						<td style="padding: 33px 15px;">Product picture</td>
						<td > <?php
							if (!empty($member->member_picture_path)>0) {
								?>
								<img width="80" height="80"  src="<?php echo base_url(); ?><?php echo $member->member_picture_path; ?>"/>
								<?php
							} else {
								?>
								<img width="80" height="80" src="<?php echo base_url(); ?>/uploads/teacher/teacher.png"/>
							<?php } ?>

						</td>
					</tr>

					<tr>
						<td>Product Name</td>
						<td> <?php echo $member->member_name; ?></td>
					</tr>
					<tr>
						<td>Product designation</td>
						<td> <?php echo $member->member_title; ?></td>
					</tr>
					<tr>
						<td>Product Phone number</td>
						<td> <?php echo $member->member_phone; ?></td>
					</tr>
					<tr>
						<td>Product email</td>
						<td> <?php echo $member->email_id; ?></td>
					</tr>
					<tr>
						<td>Product facebook Id</td>
						<td> <?php echo $member->facebook_id; ?></td>
					</tr>
					<tr>
						<td>
                            Product  twiter Id
						</td>
						<td> <?php echo $member->twiter_id; ?></td>
					</tr>
					<tr>
						<td>Product  linked Id </td>
						<td> <?php echo $member->linkined_id; ?></td>
					</tr>

					<tr>
						<td><a class="btn btn-instagram pull-left" href="<?php echo base_url(); ?>member-list">Back</a> </td>
						<td> </td>
					</tr>

					</tbody>

				</table>
			</div>


		</div>
	</div>


</div>
</div>
