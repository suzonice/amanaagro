
<div class="col-md-offset-0 col-md-12">
<div class="box  box-success">
	<div class="box-header with-border">
		<h3 class="box-title"><a class="btn btn-success" href="<?php echo base_url();?>marketer-create"><i class="fa fa-plus-circle"></i>Add new</span></a></h3>


	</div>
	<div class="box-body">
<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
			<tr>
				<th>Serial</th>
				<th>Picture</th>
				<th>Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Job</th>
				<th>Address</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			<?php if (isset($marketers)):

				$count = 1;
				//var_dump($count);exit();
				foreach ($marketers as $member):

					?>
					<tr>
						<td><?php echo $count; ?></td>
                        <td><?php
                            if(!empty($member->marketer_picture_path)):
                                ?>
              <img width="70" height="50" src="<?php echo base_url(); echo $member->marketer_picture_path; ?>"/>
                            <?php
                            else:
                                ?>
                                <img width="70" height="50"  src="<?php echo base_url() ?>uploads/teacher/teacher.png"/>
                            <?php endif;
                            ?></td>
						<td><?php echo $member->marketer_name; ?></td>
						<td><?php echo $member->marketer_mobile; ?></td>
						<td><?php echo $member->marketer_email; ?></td>
						<td><?php echo $member->name; ?></td>
						<td><?php echo $member->marketer_address; ?></td>



<td>
<!--    <a target="_blank" href="--><?php //echo base_url() ?><!--member-view/--><?php //echo $member->marketer_id; ?><!--"-->
<!--    <span class="glyphicon glyphicon-eye-open btn btn-info"></span>-->
<!--    </a>	-->
	<a href="<?php echo base_url() ?>marketer-edit/<?php echo $member->marketer_id; ?>"
    <span class="glyphicon glyphicon-edit btn btn-success"></span>
    </a>
							<a href="<?php echo base_url() ?>marketer-delete/<?php echo $member->marketer_id; ?>"
							   onclick="return confirm('Are you want to delete this information :press Ok for delete otherwise Cancel')">
								<span class="glyphicon glyphicon-trash btn btn-danger"></span>
							</a>


						</td>

					</tr>

					<?php
					$count++;
				endforeach;
			endif; ?>

			</tbody>

		</table>


	</div>
	</div>

</div>
</div>
