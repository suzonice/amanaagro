<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Marketing officer Name<span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($marketer)) echo $marketer->marketer_name; ?>"
               class="form-control" name="marketer_name" placeholder="Provide member name">
        <span id="spanId" style="color:red"> <?php echo form_error('marketer_name');  ?></span>

        <input type="hidden" id="field-1" name="marketer_id"
               value="<?php if (isset($marketer)) echo $marketer->marketer_id; ?>" class="form-control">
    </div>
</div>


<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Marketing officer Phone number<span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($marketer)) echo $marketer->marketer_mobile; ?>"
               class="form-control" name="marketer_mobile" placeholder="Provide member phone">

        <span id="spanId" style="color:red"> <?php echo form_error('marketer_mobile');  ?></span>
    </div>
</div>
<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Marketing officer email <span style="color:red"></span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($marketer)) echo $marketer->marketer_email; ?>"
               class="form-control" name="marketer_email" placeholder="Provide member email">

    </div>
</div>



<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Marketing officer job location  <span style="color:red">  *</span> </label>
	<div class="col-sm-7">
		<select  required name="district_id"  class="form-control select2">
			<option value="">Select district</option>
			<?php foreach ($districts as $cat): ?>

				<option

					<?php $selected=isset($marketer->district_id) ? $marketer->district_id==$cat->district_id ? 'selected="selected"':"" :"";  echo $selected;  ?>
					value="<?php  echo $cat->district_id;?>"><?php  echo $cat->name;?></option>

			<?php endforeach; ?>

		</select>
		<span style="color:red"></span>
	</div>

</div>

<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Marketing officer address <span style="color:red"></span></label>

	<div class="col-sm-8">
		<input type="text" id="field-1" value="<?php if (isset($marketer)) echo $marketer->marketer_address; ?>"
			   class="form-control" name="marketer_address" placeholder="Provide Marketing officer address">

	</div>
</div>
<?php if(!empty($marketer->marketer_picture_path)):?>
	<div class="form-group">
		<label for="field-1" class="col-sm-3 control-label">Marketing officer picture</label>
		<div class="col-sm-8">
			<img width="70" height="50" src="<?php echo base_url(); if(isset($marketer)){ echo $marketer->marketer_picture_path;} ?>"/>

			<input type="hidden"  class="form-control" name="old_marketer_picture_path" value="<?php  if(isset($marketer)){ echo $marketer->marketer_picture_path;} ?>">
		</div>
	</div>
<?php endif;?>
<div class="form-group">
	<label for="field-1" class="col-sm-3 control-label">Marketing officer picture</label>
	<div class="col-sm-8">
		<input type="file"  class="form-control" name="marketer_picture_path">
	</div>
</div>






					
