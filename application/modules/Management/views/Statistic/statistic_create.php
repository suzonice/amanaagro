<div class="col-md-offset-2 col-md-6">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title"><?php if (isset($title)) echo $title ?></h3>


		</div>
		<div class="box-body">
            <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>

		<form action="<?php echo base_url()?>statistic-update" class="form-horizontal"  method="post" >

		<?php $this->load->view('statistic_form');?>
		
		</div>

			<div class="box-footer">
				<input type="submit" class="btn btn-success pull-right" value="Update"/>
				<a class="btn btn-danger pull-left" href="<?php echo base_url();?>category-list">Cancel</a>

			</div>
		</form>
        </div>
