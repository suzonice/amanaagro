<div class="box box-success ">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $title; ?></h3>


	</div>
	<div class="box-body">

		<form action="<?php echo base_url() ?>product-update" class="form-horizontal" method="post" enctype="multipart/form-data">
			<?php $this->load->view('products_form'); ?>

	</div>

	<div class="box-footer">
		<input type="submit" class="btn btn-success pull-right" value="Update"/>
		<a class="btn btn-danger " href="<?php echo base_url();?>product-list">Cancel</a>

	</div>
	</form>
</div>
