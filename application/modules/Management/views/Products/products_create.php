
<div class="col-md-offset-1 col-md-10">


<div class="box box-success  ">
	<div class="box-header with-border">
		<h3 class="box-title"><?php if (isset($title)) echo $title ?></h3>

	</div>
	<div class="box-body">

		<form action="<?php echo base_url() ?>product-save" class="form-horizontal" method="post" enctype="multipart/form-data">
			<span style="text-align:center;color:red"><?php echo validation_errors(); ?></span>
			<?php $this->load->view('products_form'); ?>


	</div>

	<div class="box-footer">
		<input type="submit" class="btn btn-success pull-right" value="Save"/>
		<a class="btn btn-danger " href="<?php echo base_url(); ?>product-list">Cancel</a>

	</div>
	</form>
</div>
</div>
