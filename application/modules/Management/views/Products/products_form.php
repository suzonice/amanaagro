<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Product Name<span style="color:red"> *</span></label>

	<div class="col-sm-7">
		<input  type="text" id="field-1" class="form-control" name="product_name"
			   value="<?php if (isset($product)) echo $product->product_name; ?>"
			   placeholder="Provide product name">
		<input type="hidden" name="product_id" value="<?php if (isset($product)) echo $product->product_id; ?>">
	</div>
</div>

<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">product poriman <span style="color:red"></span></label>

	<div class="col-sm-7">
		<input  type="text" id="field-1" class="form-control" name="product_poriman"
			   value="<?php if (isset($product)) echo $product->product_poriman; ?>"
			   placeholder="Provide product poriman">
	</div>
</div>
<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Product element <span style="color:red"> *</span></label>

	<div class="col-sm-7">
		<input  type="text" id="field-1" class="form-control" name="product_element"
			   value="<?php if (isset($product)) echo $product->product_element; ?>"
			   placeholder="Provide product element ">
	</div>
</div>
<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Product  work <span style="color:red"></span></label>

	<div class="col-sm-7">
        <textarea class="form-control" name="product_work" rows="4" ><?php if (isset($product)) echo $product->product_work;?></textarea>

	</div>
</div>

<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Product killing state<span style="color:red"> *</span></label>

	<div class="col-sm-7">
		<input  type="text" id="field-1" class="form-control" name="product_kill"
			   value="<?php if (isset($product)) echo $product->product_kill; ?>"
			   placeholder="Provide Product  killing state">
	</div>
</div>


<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">product akor poti <span style="color:red"></span></label>

	<div class="col-sm-7">
		<input  type="text" id="field-1" class="form-control" name="product_akor"
			   value="<?php if (isset($product)) echo $product->product_akor; ?>"
			   placeholder="provide Product akor">
	</div>
</div>
<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Product  10 liter panite </label>

	<div class="col-sm-7">
		<input type="text" id="field-1" class="form-control" name="product_liter"
			   value="<?php if (isset($product)) echo $product->product_liter; ?>"
			   placeholder="Product product liter">
	</div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">product category   <span style="color:red">  *</span> </label>
	<div class="col-sm-7">
		<select  required name="category_id"  class="form-control">
			<option value="">Select category name</option>
			<?php foreach ($category as $cat): ?>

	<option

		<?php $selected=isset($product->category_id) ?$product->category_id==$cat->category_id ? 'selected="selected"':"" :"";  echo $selected;  ?>
		value="<?php  echo $cat->category_id;?>"><?php  echo $cat->category_name;?></option>

				<?php endforeach; ?>

		</select>
        <span style="color:red"></span>
	</div>

</div>

<?php if(!empty($product->product_images)):?>
	<div class="form-group">
		<label for="field-1" class="col-sm-4 control-label">Product  picture</label>
		<div class="col-sm-7">
			<img width="70" height="50" src="<?php echo base_url(); if(isset($product)){ echo $product->product_images;} ?>"/>

			<input type="hidden"  class="form-control" name="old_product_images" value="<?php  if(isset($product)){ echo $product->product_images;} ?>">
		</div>
	</div>
<?php endif;?>


<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Product picture</label>

	<div class="col-sm-7">
		<input type="file" id="field-1" class="form-control" name="product_images">
	</div>
</div>
