<div class="col-md-offset-1 col-md-10">
	<div class="box box-success">
		<div class="box-header with-border">
		</div>
		<div class="box-body">
			<form action="#" class="form-horizontal" method="post">

				<div class="container"><h4><?php if (isset($title)) echo $title ;?> </h4></div>
							<div class="form-group">
								<label for="shiftName" class="col-sm-4 control-label"> Name:</label>
								<div class="col-md-4">
									<select required name="marketer_id" id="marketer_id" class="form-control select2">
										<option value="">Select  name</option>
										<?php
										if (isset($marketers)):
											foreach ($marketers as $marketer):
												?>
												<option
													value="<?php echo $marketer->member_id;?>" > <?php echo $marketer->member_name ;?> </option>					<?php endforeach; else : ?>
											<option value="">Registration first  name</option>
										<?php endif; ?>
									</select>
									<h4 id="studentExit" ></h4>
								</div>
							</div>
							<div class="form-group">
								<label for="shiftName" class="col-sm-4 control-label">  Password:</label>
								<div class="col-sm-4">

									<input type="password"  required name="user_password" id="studentPassword" placeholder="Provide password" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label for="shiftName" class="col-sm-4 control-label">User role:</label>
								<div class="col-sm-4">
									<select required name="user_role" id="studentUserRole" class="form-control ">
										<option value="">Select user role</option>

										<option value="2" > Editor </option>
										<option value="1" > Admin </option>
									</select>
								</div>
							</div>

				<div class="box-footer">
					<input type="submit" class="btn btn-success pull-right" value="Save"/>
					<a class="btn btn-danger " href="<?php echo base_url();?>user-list">Cancel</a>

				</div>
			</form>
		</div>
	</div>
</div>

	<script>
		$(function () {
			$(":submit").click(function (e) {
				e.preventDefault();

				var marketer_id=$("#marketer_id").val();
				var user_role=$("#studentUserRole").val();
				var user_password=$("#studentPassword").val();
				if(marketer_id.length ==0 ||  user_role.length ==0 || user_password.length ==0  ) {
 alert("Fill up all the fields ");
				} else {
					$.ajax({

						type: "POST",
						data: {marketer_id: marketer_id, user_role: user_role, user_password: user_password},
						url: '<?php echo base_url()?>UserController/store',
						success: function (data) {
							$("#studentExit").html(data);
							$("#studentExit").fadeOut(5000);
							$("#marketer_id").val("");
							return false;
						}
					});
				}


			});


			$("#marketer_id").change(function () {

				var student_id=$("#marketer_id").val();
				$.ajax({
					type:"POST",
					data:{student_id:student_id},
					url:'<?php echo base_url()?>UserController/studentCheck',
					success:function (data) {
						if (data.length > 1) {
							$("#studentExit").html(data);
							$(':submit').prop('disabled', true);
							return false;
						} else {
							$("#studentExit").html("");
							$(':submit').prop('disabled', false);
							return false;
						}
					}
				});


			});



			});





	</script>
