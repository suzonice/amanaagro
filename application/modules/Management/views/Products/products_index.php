
<div class="col-md-offset-0 col-md-12">

<div class="box  box-success">
	<div class="box-header with-border">
		<h3 class="box-title"><a class="btn btn-success" href="<?php echo base_url();?>product-create"><i class="fa fa-plus-circle"></i>Add new</span></a></h3>


	</div>
	<div class="box-body">
		<div class="table-responsive">
		<table id="example1" class="table table-bordered table-hover  ">
			<thead>
			<tr>
				<th>No:</th>
				<th>ProductPicture </th>
				<th>ProductName</th>
				<th>ProductEelement</th>
				<th>ProductCategory</th>

				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			<?php if (isset($products)):

				$count = 1;
				//var_dump($count);exit();
				foreach ($products as $product):

					?>
					<tr>
						<td><?php echo $count; ?></td>
						<td><?php
							if(isset($product->product_images)):
								?>
								<img width="70" height="50" src="<?php echo base_url();  echo $product->product_images; ?>"/>
							<?php
							else:
								?>
								<img width="70" height="50"  src="<?php echo base_url() ?>uploads/logo.png"/>
							<?php endif;
							?></td>
						<td><?php echo $product->product_name; ?></td>
						<td><?php echo $product->product_element; ?></td>
						<?php foreach ($category as $cat):
							if($cat->category_id==$product->category_id):
							?>
						<td><?php echo $cat->category_name; ?></td>
						<?php endif;endforeach;?>

						<td>
							<a title="view" href="<?php echo base_url() ?>product-view/<?php echo $product->product_id;  ?>"
							<span class=" glyphicon glyphicon-user btn btn-primary"></span>
							</a>
							<a title="edit" href="<?php echo base_url() ?>product-edit/<?php echo $product->product_id;  ?>"
							<span class="glyphicon glyphicon-edit btn btn-success"></span>
							</a>
							<a title="delete" href="<?php echo base_url() ?>product-delete/<?php echo $product->product_id;  ?>"
							   onclick="return confirm('Are you want to delete this information :press Ok for delete otherwise Cancel')">
								<span class="glyphicon glyphicon-trash btn btn-danger"></span>
							</a>
						</td>

					</tr>

					<?php
					$count++;
				endforeach;
			endif; ?>

			</tbody>

		</table>


	</div>
	</div>

</div>
</div>
