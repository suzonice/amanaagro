<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Link Title<span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($link)) echo $link->link_title; ?>"
               class="form-control" name="link_title" placeholder="Provide link title">

        <input type="hidden" id="field-1" name="link_id"
               value="<?php if (isset($link)) echo $link->link_id; ?>" class="form-control">
    </div>
</div>


<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Link address<span style="color:red">*</span></label>

	<div class="col-sm-8">
		<input type="text" id="field-1" value="<?php if (isset($link)) echo $link->link; ?>"
			   class="form-control" name="link" placeholder="Provide link address">


	</div>
</div>

