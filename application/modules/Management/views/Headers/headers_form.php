<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Header Name<span style="color:red"> *</span></label>

    <div class="col-sm-8">
        <input type="text"   name="header_one" value="<?php if (isset($header)){ echo $header->header_one; }?>"
               class="form-control" >


    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Header Slug<span style="color:red"> *</span></label>

    <div class="col-sm-8">
        <input type="text" name="header_two" value="<?php if (isset($header)) echo $header->header_two; ?>"
               class="form-control"  >

    </div>
</div>


<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Company Mobile <span style="color:red"> *</span></label>

    <div class="col-sm-8">
        <input type="text" name="header_mobile"  value="<?php if (isset($header)) echo $header->header_mobile; ?>"
               class="form-control" >

    </div>
</div>


<div class="form-group">
    <label for="field-1" class="col-sm-4 control-label">Company Email<span style="color:red"> *</span></label>

    <div class="col-sm-8">
        <input type="text" name="header_email" value="<?php if (isset($header)) echo $header->header_email; ?>"
               class="form-control"  >

    </div>
</div>
<div class="form-group">
	<label for="field-1" class="col-sm-4 control-label">Google location<span style="color:red"> *</span></label>

	<div class="col-sm-8">
		<textarea type="text" name="header_location" value=""
			   class="form-control"  ><?php if (isset($header)) echo $header->header_location; ?>
		</textarea>

	</div>
</div>


<?php if(!empty($header->logo)):?>
	<div class="form-group">
		<label for="field-1" class="col-sm-3 control-label">logo </label>
		<div class="col-sm-8">
			<img width="70" height="50" src="<?php echo base_url(); if(isset($header)){ echo $header->logo;} ?>"/>

			<input type="hidden"  class="form-control" name="old_logo" value="<?php  if(isset($header)){ echo $header->logo;} ?>">
		</div>
	</div>
<?php endif;?>
<div class="form-group">
	<label for="field-1" class="col-sm-3 control-label">logo </label>
	<div class="col-sm-8">
		<input type="file"  class="form-control" name="logo">
	</div>
</div>




