<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Galery Image Name<span style="color:red">*</span></label>

    <div class="col-sm-8">
        <input type="text" id="field-1" value="<?php if (isset($galery)) echo $galery->galery_name; ?>"
               class="form-control" name="galery_name" placeholder="Provide Galery name">

        <input type="hidden" id="field-1" name="galery_id"
               value="<?php if (isset($galery)) echo $galery->galery_id; ?>" class="form-control">
    </div>
</div>
<?php if(!empty($galery->galery_image_path)):?>
	<div class="form-group">
		<label for="field-1" class="col-sm-3 control-label">Galery picture</label>
		<div class="col-sm-8">
			<img width="70" height="50" src="<?php echo base_url(); if(isset($galery)){ echo $galery->galery_image_path;} ?>"/>

			<input type="hidden"  class="form-control" name="old_galery_image_path" value="<?php  if(isset($galery)){ echo $galery->galery_image_path;} ?>">
		</div>
	</div>
<?php endif;?>
<div class="form-group">
	<label for="field-1" class="col-sm-3 control-label">Galery picture</label>
	<div class="col-sm-8">
		<input type="file"  class="form-control" name="galery_image_path">
	</div>
</div>
