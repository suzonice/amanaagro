<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('MainModel');
		$this->load->helper('security');
		}

	public function index()
	{
		$userRole=$this->session->userdata('user_role');
		if($userRole !=null){
			redirect('dashboard');
		}
		$this->load->view('login');
	}
	public function  userRole(){

		$data['main'] = "Users";
		$data['active'] = "view user";
		$data['classSections'] = $this->MainModel->getAllData('', 'classreg_section_com', '*', 'classreg_section_id DESC');
		$data['pageContent'] = $this->load->view('users/users_index', $data, true);
		$this->load->view('layouts/main', $data);
	}

	public function create()
	{

		$data['title'] = "User registration form ";
		$data['main'] = "Users";
		$data['active'] = "Add user";
		$data['students'] = $this->MainModel->getAllData('', 'students', '*', 'student_id DESC');
		$data['teachers'] = $this->MainModel->getAllData('', 'teachers', '*', 'teacher_id DESC');
		$data['marketers'] = $this->MainModel->getAllData('marketer_status=1', 'marketer', '*', 'marketer_id DESC');
		$data['pageContent'] = $this->load->view('users/users_create', $data, true);
		$this->load->view('layouts/main', $data);
	}



	public function loginCheck()
	{

		$email = $this->input->post('user_email');
		$password = $this->input->post('user_password');
		$this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('user_password', 'password', 'required');
		if ($this->form_validation->run()) {			
			$accountantQuery ="select  *  from  user join  members on members.member_id=user.member_id
where user.user_password='$password' and members.email_id='$email' and user.user_status=1";
			$accountantResult=$this->MainModel->SingleQueryData($accountantQuery);

			 if ($accountantResult) {
				$data['user_role'] = $accountantResult->user_role;
				$data['name'] = $accountantResult->marketer_name;
				$data['marketer_id'] = $accountantResult->marketer_id;
				$data['picture_path'] = $accountantResult->marketer_picture_path;
				$data['user_join_date'] = date('j,M,Y',strtotime($accountantResult->user_join_date));

				$this->session->set_userdata($data);
			//	var_dump($data);exit();
				redirect('dashboard');
			}
			else {
				$data['error'] = "Invalid User Name or password  !!!!";
				$this->session->set_userdata($data);
				redirect('admin');
			}


		} else {

			$this->load->view('login');
		}


	}

	public function store()
	{
		date_default_timezone_set('Asia/Dhaka');

		$marketer_id = $this->input->post('marketer_id');
		if(strlen($marketer_id)>0){
			$data['member_id'] = $marketer_id;
			$data['user_password'] = $this->input->post('user_password');
			$data['user_join_date'] = date('Y-m-d');
			$data['user_role'] = $this->input->post('user_role');
			$result = $this->MainModel->insertData('user', $data);
			if ($result) {
				echo  "<span style='color:green'>User role  added  successfully !!!!</span>";

			}
			else {
				echo "<span style='color:red'>User role  does not add  successfully !!!!</span>";

			}

		}

	}

	public function studentCheck()
	{

		$student_id = $this->input->post('student_id');
		$query="select * from user where user.member_id=$student_id and user.user_status=1";
		$result = $this->MainModel->AllQueryDalta($query);
		if ($result) {
			echo  "<span style='color:red'>This User role  exit !!!!!!</span>";

		}
		else {
			echo  "";

		}
	}

	public function teacherCheck()
	{

		$teacher_id = $this->input->post('teacher_id');
		$query="select * from user where user.teacher_id=$teacher_id and user.user_status=1";
		$result = $this->MainModel->AllQueryDalta($query);
		if ($result) {
			echo  "<span style='color:red'>This Teacher user role  exit !!!!!!</span>";

		}
		else {
			echo  "";

		}
	}

	public function AccountantCheck()
	{

		$teacher_id = $this->input->post('marketer_id');
		$query="select * from user where user.marketer_id=$teacher_id and user.user_status=1";
		$result = $this->MainModel->AllQueryDalta($query);
		if ($result) {
			echo  "<span style='color:red'>This Accountant user role  exit !!!!!!</span>";

		}
		else {
			echo  "";

		}
		exit();
	}
	public function studentList()
	{


		$classreg_section_id  = $this->input->post('classreg_section_id');

			$query="select student_name ,classreg_section_name,student_email, students.student_phone,user.student_id as studentId,user.* from user 
join students on students.student_id=user.student_id 
join student_classreg_section_com on student_classreg_section_com.student_id=students.student_id 
join classreg_section_com on classreg_section_com.classreg_section_id=student_classreg_section_com.classreg_section_id
where student_classreg_section_com.student_classreg_section_isActive=1 and classreg_section_com.classreg_section_id=$classreg_section_id";
		$data['students'] = $this->MainModel->AllQueryDalta($query);

		echo json_encode($data);
	}

	public function studentActiveInactive()
	{


		$studentActiveId  = $this->input->post('studentActiveId');
		$studentInActiveId  = $this->input->post('studentInActiveId');
		if(!empty($studentActiveId)){
			$data['user_status']=0;
			$result = $this->MainModel->updateData('student_id', $studentActiveId, 'user', $data);

//			$query="update user set user.user_status=0 where user.student_id=$studentActiveId ";
//			$this->MainModel->AllQueryDalta($query);
$data['data'] ="Student user role Inactive successfullY !!!!!!";

		}else{
			$data['user_status']=1;
			$result = $this->MainModel->updateData('student_id', $studentInActiveId, 'user', $data);

//			$query="update user set user.user_status=1 where user.student_id=$studentInActiveId ";
//			 $this->MainModel->AllQueryDalta($query);
			$data['data'] = "Student user role Active successfullY !!!!!!";

		}
echo json_encode($data);

	}


	public function show($id)
	{

	}

	public function logOut()
	{
		$this->session->unset_userdata('user_role');
		$data['message']="You are successfully Logout!!!!!!!!!";
		$this->session->set_userdata($data);
		redirect('admin');


	}
	public  function websitelogout(){

		$this->session->unset_userdata('user_role');
		$data['message']="You are successfully Logout!!!!!!!!!";
		$this->session->set_userdata($data);
		redirect('login');


	}

	public function update()
	{



	}

	public function loginForm()

	{
		$userRole=$this->session->userdata('user_role');
		if($userRole !=null){
			redirect('slider-list');
		}
		$this->load->view('adminwebsite/login');

	}
	public  function websiteLogin(){
		$email = $this->input->post('user_email');
		$password = $this->input->post('user_password');
		$this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('user_password', 'password', 'required');
		if ($this->form_validation->run()) {
			$teacherQuery ="select * from user join teachers on teachers.teacher_id=user.teacher_id
where user.user_password='$password' and teachers.teacher_email='$email' and user.user_status=1";
			$teacherResult=$this->MainModel->SingleQueryData($teacherQuery);

			if ($teacherResult) {
				$data['user_role'] = $teacherResult->user_role;
				$data['teacher_full_name'] = $teacherResult->teacher_full_name;
				$data['teacher_id'] = $teacherResult->teacher_id;
				$data['teacher_picture_path'] = $teacherResult->teacher_picture_path;
				$data['user_join_date'] = date('j,M,Y',strtotime($teacherResult->user_join_date));

				$this->session->set_userdata($data);
				redirect('slider-list');
			}

			else {
				$data['error'] = "Invalid User Name or password  !!!!";
				$this->session->set_userdata($data);
				redirect('login');
			}


		} else {

			$this->load->view('login');
		}


	}
}
