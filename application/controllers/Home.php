<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('MainModel');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->helper('form');
		date_default_timezone_set('Asia/Dhaka');
		$visitor['visitor_ip']=$_SERVER['REMOTE_ADDR'];
		$visitor['visitor_date'] =date('Y-m-d');
		$result=$this->MainModel->visitorCount($visitor['visitor_ip'],$visitor['visitor_date']);
		if(empty($result)){
			$result = $this->MainModel->insertData('visitors', $visitor);

		}
	}

	public function index()
	{
		$data['sliders'] = $this->MainModel->getAllData('', 'slider', '*', 'slider_id DESC');
		$data['category'] = $this->MainModel->getAllData('', 'category', '*', 'category_id DESC');
		$data['members'] = $this->MainModel->getAllData('', 'members', '*', 'member_id ASC');
		$data['galery'] = $this->MainModel->getAllData('', 'galery', '*', 'galery_id ASC');
		$data['links'] = $this->MainModel->getAllData('', 'link', '*', 'link_id ASC');
		$data['respons'] = $this->MainModel->getAllData('', 'respons', '*', 'respons_id ASC');
		$data['statistics'] = $this->MainModel->getSingleData('statistic_id', 1, 'statistics', '*');
		$data['setting'] = $this->MainModel->getSingleData('satting_id', 1, 'setting', '*');

		$lastMonth = date('Y-m', strtotime("last month"));
		$today = date('Y-m-d');
		$lastYear = date('Y', strtotime("last year"));
		$thisYear = date('Y', strtotime("this year"));
		$thisMonth = date('Y-m', strtotime("this month"));
		$lastWeek = date('Y-m', strtotime("last week"));

		$previous_week = strtotime("-1 week +1 day");

		$start_week = strtotime("last sunday midnight",$previous_week);
		$end_week = strtotime("next saturday",$start_week);

		$start_week = date("Y-m-d",$start_week);
		$end_week = date("Y-m-d",$end_week);


		$lastYearQuery="select count(visitor_id) as year from  visitors 
where visitors.visitor_date like '$lastYear%'";
		$data['lastYearQueryData']=$this->MainModel->QuerySingleData($lastYearQuery);

		$thisYearQuery="select count(visitor_id) as thisyear from  visitors 
where visitors.visitor_date like '$thisYear%'";
		$data['thisYearQueryData']=$this->MainModel->QuerySingleData($thisYearQuery);

		$lastMonthQuery="select count(visitor_id) as lastMonth from  visitors 
where visitors.visitor_date like '$lastMonth%'";
		$data['lastMonthQueryData']=$this->MainModel->QuerySingleData($lastMonthQuery);

		$thisMonthQuery="select count(visitor_id) as thisMonth from  visitors 
where visitors.visitor_date like '$thisMonth%'";
		$data['thisMonthQueryData']=$this->MainModel->QuerySingleData($thisMonthQuery);

		$lastWeekQuery="select count(visitor_id) as lastWeek from  visitors 
where visitors.visitor_date   between '$start_week' and  '$end_week'";
		$data['lastWeekQueryData']=$this->MainModel->QuerySingleData($lastWeekQuery);

		$todayQuery="select count(visitor_id) as today from  visitors 
where visitors.visitor_date = '$today'";
		$data['todayQueryData']=$this->MainModel->QuerySingleData($todayQuery);

		$totalQuery="select count(visitor_id) as total from  visitors ";
		$data['totalQueryData']=$this->MainModel->QuerySingleData($totalQuery);





		$data['main'] =$this->load->view('website/maincontent',$data,true);
		 $this->load->view('website/home',$data);
	}

	public function officer()
	{
		$query="select marketer.*, districts.name as d_name from marketer left
join districts on districts.district_id=marketer.district_id
";
		$data['marketers']  = $this->MainModel->AllQueryDalta($query);
//		var_dump($data['marketers']);exit();
//		$data['marketers'] = $this->MainModel->getAllData('', 'marketer', '*', 'marketer_id DESC');

		$data['main'] =$this->load->view('website/officer',$data,true);
		$this->load->view('website/home',$data);
	}


	public function gallery()
	{

//		var_dump($data['marketers']);exit();
		$data['galery'] = $this->MainModel->getAllData('', 'galery', '*', 'galery_id DESC');
		$data['main'] =$this->load->view('website/galery',$data,true);
		$this->load->view('website/home',$data);
	}


	public function contact()
	{
		$data['header'] = $this->MainModel->getAllData('', 'headers', '*', 'header_id DESC');
		$data['main'] =$this->load->view('website/contact',$data,true);
		$this->load->view('website/home',$data);
	}


	public function productGelery()
	{
		$data['products'] = $this->MainModel->getAllData('', 'products', '*', 'product_id DESC');
		$data['main'] =$this->load->view('website/product_gelery',$data,true);
		$this->load->view('website/home',$data);
	}

	public function member()
	{
		$data['members'] = $this->MainModel->getAllData('', 'members', '*', 'member_id DESC');
		$data['main'] =$this->load->view('website/member',$data,true);
		$this->load->view('website/home',$data);
	}
	public function product()
	{
		//$data['products'] = $this->MainModel->getAllData('', 'products', '*', 'category_id DESC');
		$query="select * from products join category on category.category_id=products.category_id order by category.category_id desc";
		$data['products'] = $this->MainModel->AllQueryDalta($query);

		$data['catogories'] = $this->MainModel->getAllData('', 'category', '*', 'category_id DESC');

		$data['main'] =$this->load->view('website/products',$data,true);
		$this->load->view('website/home',$data);
	}


	public function Mail()
    {

		$first = $this->input->post('first');
		$last = $this->input->post('last');
	$from_email = $this->input->post('email');
		$mes = $this->input->post('mes');
		$name=$first.'-'.$last;
		$to_email = "amanaagro@gmail.com";
		$this->load->library('email');
		$this->email->set_header($from_email, $from_email);

		$this->email->from($from_email, $name);
		$this->email->to($to_email);
		$this->email->subject('Assalamu alikum i seed email from website receive it');
		$this->email->message($mes);
		if($this->email->send())
			echo "Email sent successfully.";
		else
			echo "Email  not sent successfully.";
	}




	public function edit($id)
	{

		$data['Category'] = $this->MainModel->getSingleData('category_id', $id, 'category', '*');
		$memberId = $data['Category']->category_id;
		if (isset($memberId)) {
			$data['title'] = "Category update page ";
			$data['main'] = "Category";
			$data['active'] = "Update Category";
			$data['pageContent'] = $this->load->view('management/Category/category_edit', $data, true);
			$this->load->view('layouts/main', $data);
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('category-list');
		}

	}

	public function update()
	{
		$memberId = $this->input->post('category_id');
		// check if the element exists before trying to edit it
		$memberData = $this->MainModel->getSingleData('category_id', $memberId, 'category', '*');
		$memberId = $memberData->category_id;

		if (isset($memberId)) {
            $data['category_name'] = $this->input->post('category_name');

            $this->form_validation->set_rules('category_name', 'Category name','required' );

			if ($this->form_validation->run()) {
				$result = $this->MainModel->updateData('category_id', $memberId, 'category', $data);
				if ($result) {
					$this->session->set_flashdata('message', "Category updated successfully !!!!");
					redirect('category-list');
				}
			} else {

               $this->edit();
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to edit does not exist.");
			redirect('member-list');
		}

	}

	public function destroy($id)
	{
        $memberData = $this->MainModel->getSingleData('category_id', $id, 'category', '*');
		$memberId = $memberData->category_id;
		if (isset($memberId)) {
			$result = $this->MainModel->deleteData('category_id', $memberId, 'category');
			if ($result) {
				$this->session->set_flashdata('message', "Category deleted successfully !!!!");
				redirect('category-list');
			}
		} else {
			$this->session->set_flashdata('error', "The element you are trying to delete does not exist.");
			redirect('category-list');
		}
	}
}
