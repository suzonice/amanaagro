<?php
if (!defined('BASEPATH'))    exit('No direct script access allowed');

class MainModel extends CI_Model {

   public function __construct() {
        parent::__construct();
    }
	
	  public function getAllData($condition='', $tableName='', $selectQuery='', $order='' )
    {
        $this->db->select($selectQuery);
        if ($condition): $this->db->where($condition);
        endif;
		if($order):$this->db->order_by($order);
		endif;
        return $this->db->get($tableName)->result();

    }
	function SingleQueryData($query)
	{
		return $this->db->query($query)->row();
	}

    public function getSingleData($field,$condition, $tableName, $selectQuery)
    {
        $this->db->select($selectQuery);
        $this->db->where($field,$condition);
        return $this->db->get($tableName)->row();

    }

	public function getSingleDataArrayType($field,$condition, $tableName, $selectQuery)
	{
		$this->db->select($selectQuery);
		$this->db->where($field,$condition);
		return $this->db->get($tableName)->row_array();

	}

	public function getDataRow($field,$condition, $tableName, $selectQuery)
	{
		$this->db->select($selectQuery);
		$this->db->where($field,$condition);
		return $this->db->get($tableName)->num_rows();

	}

    function insertData($tableName,$data)
    {
        return $this->db->insert($tableName, $data);
    }

    function deleteData($field,$condition, $tableName)
    {
         $this->db->where($field,$condition);
        return $this->db->delete($tableName);
    }

	function AllQueryDalta($query)
	{
		return $this->db->query($query)->result();
	}

    function updateData($field,$condition,$tableName,$data)
    {
      
        $this->db->where($field,$condition);
        $result= $this->db->update($tableName, $data);
		if($result){

			return true;
		}
		else {

			return false;
		}
    }


	function visitorCount($ip,$date)
	{
		$this->db->where('visitor_ip', $ip);
		$this->db->where('visitor_date', $date);
		$insert_id = $this->db->get('visitors')->row();
		return $insert_id;
	}
	function QuerySingleData($query)
	{
		return $this->db->query($query)->row();
	}



}
	

   
