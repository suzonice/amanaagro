<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'WelcomeController/index';
$route['default_controller'] = 'Home';
$route['admin'] = 'UserController';
$route['login-check'] = 'UserController/loginCheck';
$route['logout'] = 'UserController/logOut';

/************************ Website  ************************/
$route['merketing-officer'] = 'Home/officer';
$route['gallery-image'] = 'Home/gallery';
$route['product-picture'] = 'Home/productGelery';
$route['member'] = 'Home/member';
$route['product'] = 'Home/product';
$route['contact-us'] = 'Home/contact';
$route['dashboard'] = 'WelcomeController/index';


/************************ product  ************************/
$route['user-create'] = 'management/ProductsController/user';
$route['product-create'] = 'management/ProductsController/create';
$route['product-save'] = 'management/ProductsController/store';
$route['product-view/(:any)'] = 'management/ProductsController/show/$1';
$route['product-list'] = 'management/ProductsController/index';
$route['product-update'] = 'management/ProductsController/update';
$route['product-edit/(:any)'] = 'management/ProductsController/edit/$1';
$route['product-delete/(:any)'] = 'management/ProductsController/destroy/$1';

/************************ category  ************************/
$route['category-create'] = 'management/CategoryController/create';
$route['category-save'] = 'management/CategoryController/store';
$route['category-view'] = 'management/CategoryController/show';
$route['category-list'] = 'management/CategoryController/index';
$route['category-update'] = 'management/CategoryController/update';
$route['category-edit/(:any)'] = 'management/CategoryController/edit/$1';
$route['category-delete/(:any)'] = 'management/CategoryController/destroy/$1';

/************************ menber  ************************/
$route['member-create'] = 'management/MembersController/create';
$route['member-save'] = 'management/MembersController/store';
$route['member-view/(:any)'] = 'management/MembersController/show/$1';
$route['member-list'] = 'management/MembersController/index';
$route['member-update'] = 'management/MembersController/update';
$route['member-edit/(:any)'] = 'management/MembersController/edit/$1';
$route['member-delete/(:any)'] = 'management/MembersController/destroy/$1';

/************************ category  ************************/
$route['header-create'] = 'management/HeadersController/create';
$route['header-save'] = 'management/HeadersController/store';
$route['header-update'] = 'management/HeadersController/update';
$route['header-update'] = 'management/HeadersController/update';
/************************ Statistic  ************************/
$route['statistic-create'] = 'management/StatisticsController/create';
$route['statistic-save'] = 'management/StatisticsController/store';
$route['statistic-view'] = 'management/StatisticsController/show';
$route['statistic-list'] = 'management/StatisticsController/index';
$route['statistic-update'] = 'management/StatisticsController/update';
$route['statistic-edit/(:any)'] = 'management/StatisticsController/edit/$1';
$route['statistic-delete/(:any)'] = 'management/StatisticsController/destroy/$1';


/************************ slider  ************************/
$route['slider-create'] = 'management/SlidersController/create';
$route['slider-save'] = 'management/SlidersController/store';
$route['slider-view/(:any)'] = 'management/SlidersController/show/$1';
$route['slider-list'] = 'management/SlidersController/index';
$route['slider-update'] = 'management/SlidersController/update';
$route['slider-edit/(:any)'] = 'management/SlidersController/edit/$1';
$route['slider-delete/(:any)'] = 'management/SlidersController/destroy/$1';


/************************ slider  ************************/
$route['link-create'] = 'management/LinkController/create';
$route['link-save'] = 'management/LinkController/store';
$route['link-view/(:any)'] = 'management/LinkController/show/$1';
$route['link-list'] = 'management/LinkController/index';
$route['link-update'] = 'management/LinkController/update';
$route['link-edit/(:any)'] = 'management/LinkController/edit/$1';
$route['link-delete/(:any)'] = 'management/LinkController/destroy/$1';


/************************ slider  ************************/
$route['marketer-create'] = 'management/MarketersController/create';
$route['marketer-save'] = 'management/MarketersController/store';
$route['marketer-view/(:any)'] = 'management/MarketersController/show/$1';
$route['marketer-list'] = 'management/MarketersController/index';
$route['marketer-update'] = 'management/MarketersController/update';
$route['marketer-edit/(:any)'] = 'management/MarketersController/edit/$1';
$route['marketer-delete/(:any)'] = 'management/MarketersController/destroy/$1';

/************************ slider  ************************/
$route['galery-create'] = 'management/ImagesController/create';
$route['galery-save'] = 'management/ImagesController/store';
$route['galery-view/(:any)'] = 'management/ImagesController/show/$1';
$route['galery-list'] = 'management/ImagesController/index';
$route['galery-update'] = 'management/ImagesController/update';
$route['galery-edit/(:any)'] = 'management/ImagesController/edit/$1';
$route['galery-delete/(:any)'] = 'management/ImagesController/destroy/$1';




/************************ slider  ************************/
$route['respons-create'] = 'management/ResponsController/create';
$route['respons-save'] = 'management/ResponsController/store';
$route['respons-view/(:any)'] = 'management/ResponsController/show/$1';
$route['respons-list'] = 'management/ResponsController/index';
$route['respons-update'] = 'management/ResponsController/update';
$route['respons-edit/(:any)'] = 'management/ResponsController/edit/$1';
$route['respons-delete/(:any)'] = 'management/ResponsController/destroy/$1';
/************************ category  ************************/
$route['setting-create'] = 'management/SatingsController/create';
$route['setting-update'] = 'management/SatingsController/update';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
