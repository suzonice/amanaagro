

<div class="inner-banner">
	<div class="container">
		<div class="col-sm-12">
			<h2>Our Team Members</h2>
		</div>
		<div class="col-sm-12 inner-breadcrumb">
			<ul>
				<li><a href="<?php echo base_url()?>">Home</a></li>
				<li>Team Members</li>
			</ul>
		</div>
	</div>
</div>
<section class="inner-wrapper">
	<div class="inner-wrapper-main">
		<div class="row">

			<?php if(isset($members)) : foreach ($members as $member):?>
				<div class="col-md-3 col-lg-3 col-sm-4 col " style="padding: 10px">
					<div>
						<img class="img-thumbnail" width="100%" height="10%" src="<?php echo base_url();echo $member->member_picture_path;?>"/>
					</div>
					<div class="left bg-success "  style="padding:5px ">
						<h3><i class="fa fa-user"></i>   <?php echo  $member->member_name;?></h3>
						<h3><i class="fa fa-male"></i>   <?php echo  $member->member_title;?></h3>

						<h3><i class="fa fa-phone"></i>   <?php echo  $member->member_phone;?></h3>
						<h3><i class="fa fa-envelope"></i>  <?php echo  $member->email_id;?></h3>

					</div>
				</div>
			<?php endforeach;endif;?>



		</div>
	</div>
	</div>
	</div>
	<div class="call-to-action">
		<div class="container">
			<h3>Welcome to Amana Agro Science</h3>
			<p>We need some merketing officer apply now </p>
			<a href="javascript:void(0)">Apply</a> </div>
	</div>
