<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Education World</title>
	<!-- Bootstrap CSS -->
	<link href="<?php echo base_url()?>assets/fontend/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome CSS-->
	<!-- Custom CSS -->
	<link href="<?php echo base_url()?>assets/fontend/css/style.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- Animate CSS -->
	<link href="<?php echo base_url()?>assets/fontend/css/animate.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />

	<!-- Owl Carousel -->
	<link href="cssg/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="images/fav.png">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<!-- Pre Loader -->
<!--<div id="dvLobading"></div>-->
<!-- Header Start -->




<header class="navbar navbar-inverse  navbar-fixed-top wet-asphalt" role="banner">
	<div class="container">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

		<div class="collapse navbar-collapse">


			<div id="google_translate_element"></div><script type="text/javascript">
				function googleTranslateElementInit() {
					new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
				}
			</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


			<nav class="bg-success">

				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="<?php echo base_url();?>">হোম</a></li>
					<li><a href="<?php echo base_url();?>merketing-officer">মার্কেটিং অফিসার</a></li>
					<li><a href="<?php echo base_url();?>member"">সদস্য</a></li>
					<li><a href="<?php echo base_url();?>gallery-image">গ্যালারি ছবি</a></li>
					<li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">প্রোডাক্ট <i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url();?>product-picture">প্রোডাক্টের ছবি</a></li>
							<li><a href="<?php echo base_url();?>product"> প্রোডাক্টের তালিকা</a></li>

						</ul>
					</li>

					<li><a href="<?php echo base_url();?>contact-us">যোগাযোগ</a></li>
					<li><a target="_blank" href="http://amana-agro.com/login">লগইন</a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>
<header>
	<br>
	<br>
	<br>
	<div class="container">
		<!-- Logo -->
		<?php
		$headers=$this->db->get('headers')->row();
		?>
		<div class="row" style="margin-top: 38px;">
			<div class="col-md-3">
				<a href="index.html"> <img width="200" height="100" src="<?php echo base_url();echo $headers->logo;?>" style="margin-bottom: 13px;" alt="Education World"></a>
			</div>
			<div class="col-md-4">
				<h3><?php echo $headers->header_one;?></h3>
				<h5><?php echo $headers->header_two;?></h5>
			</div>
			<div class="col-md-5">
				<ul class="contact-info pull-right">


					<li><i class="fa fa-phone"></i>
						<p> <span>Call us</span><br>
							+88<?php echo $headers->header_mobile;?></p>
					</li>
					<li><i class="fa fa-envelope"></i>
						<p><span>Email Us</span><br>
							<a href="mailto:support@sbtechnosoft.com"><?php echo $headers->header_email;?></a></p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>
