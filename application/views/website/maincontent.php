
<div class="row col-md-offset-0 col-md-12">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php  $i=0;
			if(isset($sliders)):
			foreach ($sliders as $slider):
				if($i==0):
				?>

			<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" class="active"></li>
					<?php else : ?>

					<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>"></li>
			<?php endif; $i++; endforeach;endif;?>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<?php  $i=0;
			if(isset($sliders)):
			foreach ($sliders as $slider):
			if($i==0):
			?>
			<div class="item active">
				<img  src="<?php echo base_url(); echo $slider->slider_image;?>" alt="Los Angeles" style="width:1560px ;height: 400px">
				<div class="carousel-caption">
					<h3><?php echo  $slider->slider_heading;?></h3>
				</div>
			</div>
			<?php else : ?>
				<div class="item ">
					<img  src="<?php echo base_url() ; echo $slider->slider_image;?>" alt="Los Angeles" style="width:1560px ;height: 400px">
					<div class="carousel-caption">
						<h3><?php echo  $slider->slider_heading;?></h3>
					</div>
				</div>
			<?php endif; $i++; endforeach;endif;?>

		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<i style="margin-top: 150px" class="fa fa-arrow-left" aria-hidden="true"></i>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<i  style="margin-top: 150px" class="fa fa-arrow-right" aria-hidden="true"></i>
			<span class="sr-only">Next</span>
		</a>
	</div>


</div>



<!-- Banner Wrapper End -->
<!-- About Us -->
<div class="about-us">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-8">

					 <h1>আমনা এগ্রো সাইন্স সম্পর্কে </h1>
				<p><?php echo $setting->satting_about_company;?></p>
			</div>
			<div class="col-sm-12 col-md-4 pull-right hidden-sm"><img class="img-circle" src="<?php echo base_url()?>uploads/aminul.jpg" alt="World-edu">
			</div>
		</div>
	</div>
</div>
<!-- Callouts Wrapper Start -->
<div class="callouts-wrapper">
	<div class="container">
		<h2>   <span>আমনা এগ্রো সাইন্স  এর প্রোডাক্ট   ক্যাটাগরির   তালিকা </span></h2>
		<p class="center"><p><?php echo $setting->satting_about_product;?></p></p>
		<div class="row">
			<?php if(isset($category)):
			foreach ($category as $cat):
			?>
			<div class="col-sm-6 col-md-2 wow fadeIn animated" data-wow-duration="1.5s">
				<div class="callouts">
					<div class="icon"><i class="fa fa-tree"></i></div>
					<div class="content">
						<h3><?php echo $cat->category_name;?></h3>

					</div>
				</div>
			</div>
			<?php endforeach; endif;?>


		</div>
	</div>
</div>
<!-- Callouts Wrapper End -->
<!-- Satisfied Wrapper start -->
<div class="satisfied-wrapper">
	<div class="container">
		<h2>পরিসংখ্যান   <span>আমনা এগ্রো সায়েন্সের </span></h2>
		<p class="center">				<p><?php echo $setting->satting_about_satatics;?></p>
		</p>
		<div class="statistics">
			<div class="col-sm-3 counter"><i class="fa fa-list-alt" aria-hidden="true"></i>
				<div class="number animateNumber" data-num="28"><span><?php echo $statistics->statistic_branch;?></span></div>
				<p>আমাদের শাখা</p>
			</div>
			<div class="col-sm-3 counter"><i class="fa fa-user" aria-hidden="true"></i>
				<div class="number animateNumber" data-num="180"><span><?php echo $statistics->statistic_stuff;?></span></div>
				<p>আমাদের কর্মীদের সংখ্যা </p>
			</div>
			<div class="col-sm-3 counter"><i class="fa fa-users" aria-hidden="true"></i>
				<div class="number animateNumber" data-num="3600"><span><?php echo $statistics->statistic_product;?></span></div>
				<p>আমাদের পণ্যের সংখ্যা</p>
			</div>
			<div class="col-sm-3 counter"><i class="fa fa-graduation-cap" aria-hidden="true"></i>
				<div class="number animateNumber" data-num="768"><span><?php echo $statistics->statistic_client;?></span></div>
				<p>আমাদের গ্রাহক</p>
			</div>
		</div>
	</div>
</div>
<!-- satisfied Wrapper End -->

<!-- Call to Action End -->
<!-- Faculty Wrapper Start -->
<div class="team-wrapper">
	<div class="container">
		<div class="row">
			<h2>আমাদের দলের সদস্য</h2>
			<div id="owl-demo" class="owl-carousel owl-theme">
				<?php if(isset($members)): foreach ($members as $member):?>
				<div class="item">
					<div class="img-box" style="width:187px;height: 300px;"> <img style="width:200px;height: 300px;" src="<?php base_url();echo $member->member_picture_path;?>" alt="Team1" title="Team1" />
						<div class="text-center" >
							<div class="left" >
								<h5 style="color:white"><?php echo $member->member_name;?></h5><p style="color:white " class="designation muted">  <?php echo $member->member_title;?>  </p>
								<p style="margin-left:-10px"><i class="fa fa-phone">   <?php echo $member->member_phone;?></i></p>
								<p style="margin-left:-10px "><i class="fa fa-envelope">   <?php echo $member->email_id;?></i></p>
								<ul class="header-social-icons">
									<li style="float:left;list-style:none;" class="facebook"><a href="<?php echo $member->facebook_id;?>" style="width:100px;height:100px;background-color:#0001FB;color:white;padding: 10px 10px;border-radius:50%"href="javascript:void(0)" target="_blank"><i class="fa fa-facebook"></i></a></li><li style="float:left;list-style:none;" class="facebook"><a href="<?php echo $member->twiter_id;?>" style="width:100px;height:100px;background-color:blue;color:white;padding: 10px 10px;border-radius:50%"href="javascript:void(0)" target="_blank"><i class="fa fa-twitter"></i></a></li><li style="float:left;list-style:none;" class="facebook"><a href="<?php echo $member->linkined_id;?>" style="width:100px;height:100px;background-color:blue;color:white;padding: 10px 10px;border-radius:50%" href="javascript:void(0)" target="_blank"><i class="fa fa-linkedin"></i></a></li><li style="float:left;list-style:none;" class="facebook"><a  href="<?php echo $member->email_id;?>"style="width:100px;height:100px;background-color:blue;color:white;padding: 10px 10px;border-radius:50%"href="javascript:void(0)" target="_blank"><i class="fa fa-google-plus"></i></a></li>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach;endif;?>

			</div>

		</div>
	</div>

</div>
</div>
</div>
</div>
</div>
<div class="sponsers">
	<div class="container">
		<h2>আমাদের স্পনসর</span></h2>
		<div id="sponsers" class="owl-carousel owl-theme">
			<?php if(isset($respons)):foreach ($respons as $respon):?>
		<div class="item"><img src="<?php echo base_url();echo $respon->respons_image_path; ?>" alt="<?php echo base_url();echo $respon->respons_name;?>" />
		</div>
		<?php endforeach;endif;?>

		</div>
	</div>
</div>
<!-- sponsers End -->
<!-- Gallery Start -->
<div class="gal-container full-width col-md-offset-0">
	<?php  $count=0; if(isset($galery)):foreach ($galery as $gel):
		$count++;
	if($count <5):
		?>
	<div class="col-md-3 col-sm-6 co-xs-12 gal-item">
		<div class="box"><a href="javascript:void(0)" data-toggle="modal" data-target="#<?php echo $count;?>">
				<div class="caption">
					<h4><?php echo $gel->galery_name;?></h4>
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
				<img src="<?php echo base_url();echo $gel->galery_image_path;?>" alt="<?php echo $gel->galery_name;?>"> </a>
			<div class="modal fade" id="<?php echo $count;?>" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">×</span></button>
						<div class="modal-body"><img src="<?php echo base_url();echo $gel->galery_image_path;?>" alt="<?php echo $gel->galery_name;?>"></div>
						<div class="col-md-12 description">
							<h4><?php echo $gel->galery_name;?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php  endif;endforeach;endif;?>

</div>
<!-- Gallery End -->
<!-- Footer Links Start-->
