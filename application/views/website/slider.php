<div class="banner-wrapper">
	<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
		<!-- Overlay -->
		<div class="overlay"></div>
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
			<li data-target="#bs-carousel" data-slide-to="1"></li>
			<li data-target="#bs-carousel" data-slide-to="2"></li>
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item slides active">
				<div class="slide-1">
					<img src="images/1.jpg"/>

				</div>
				<div class="hero">
					<h1 class="animated1">Education World</h1>
					<h3 class="animated2">Duis aute irure dolor in reprehenderit in voluptate velit esse</h3>
				</div>
			</div>
			<div class="item slides">
				<div class="slide-2">
					<img src="images/1.jpg"/>

				</div>
				<div class="hero">
					<h1 class="animated1">Education World</h1>
					<h3 class="animated2">Duis aute irure dolor in reprehenderit in voluptate velit esse</h3>
				</div>
			</div>
			<div class="item slides">
				<div class="slide-3">
					<img src="images/1.jpg"/>
				</div>
				<div class="hero">
					<h1 class="animated1">Education World</h1>
					<h3 class="animated2">Duis aute irure dolor in reprehenderit in voluptate velit esse</h3>
				</div>
			</div>
		</div>
	</div>
</div>
