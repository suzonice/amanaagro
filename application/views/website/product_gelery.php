<div class="inner-banner">
	<div class="container">
		<div class="col-sm-12">
			<h2>Our Product Gelery</h2>
		</div>
		<div class="col-sm-12 inner-breadcrumb">
			<ul>
				<li><a href="<?php echo base_url()?>">Home</a></li>
				<li>Product Geleryr</li>
			</ul>
		</div>
	</div>
</div>
<section class="inner-wrapper">
	<div class="inner-wrapper-main">
		<div class="gal-container full-width">

			<?php $i=0; if(isset($products)): foreach ($products as $product):?>
				<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
					<div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#<?php echo $i;?>">
							<div class="caption">
								<h4><?php echo $product->product_name;?></h4>
								<i class="fa fa-search" aria-hidden="true"></i> </div>
							<img src="<?php echo base_url();echo $product->product_images;?>" alt="<?php echo $product->product_name;?>"> </a>
						<div class="modal fade" id="<?php echo $i;?>" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
									<div class="modal-body"> <img src="<?php echo base_url();echo $product->product_images;?>" alt="<?php echo $product->product_name;?>"> </div>
									<div class="col-md-12 description">
										<h4><?php echo $product->product_name;?></h4>								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php $i++; endforeach; endif;?>

		</div>
	</div>
</section>
