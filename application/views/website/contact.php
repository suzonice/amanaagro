
<div class="inner-banner">
	<div class="container">
		<div class="col-sm-12">
			<h2>Contact Us</h2>
		</div>
		<div class="col-sm-12 inner-breadcrumb">
			<ul>
				<li><a href="<?php echo base_url()?>">Home</a></li>
				<li>Contact us</li>
			</ul>
		</div>
	</div>
</div>
<div class="alert alert-success col-md-offset-2 col-md-6" id="messegeEmail" style="margin-top:50px">
	<strong>Success!</strong><span id="messegeSent"></span>
</div>

<section class="inner-wrapper contact-wrapper">
	<div class="container">
		<div class="row">
			<div class="inner-wrapper-main">
				<div class="contact-address">
					<div class="col-sm-12 col-md-6 no-space-right">
						<div class="col-sm-6 contact"> <i class="fa fa-map-marker"></i>
							<p><span>Address</span><br>
								<?php if(isset($header[0])){echo $header[0]->header_two;}?></p>
						</div>
						<div class="col-sm-6 contact white"> <i class="fa fa-phone"></i>
							<p><span>Phone Number</span><br>
								<?php if(isset($header[0])){echo $header[0]->header_mobile;}?></p>
						</div>
						<div class="col-sm-6 contact white"><i class="fa fa-mobile-phone"></i>
							<p><span>Customer Care</span><br>
								+8801918-359790</p>
						</div>
						<div class="col-sm-6 contact"> <i class="fa fa-envelope"></i>
							<p><span>Email</span><br>
								<a href="mailto:support@yourdomain.com"><?php if(isset($header[0])){echo $header[0]->header_email;}?></a></p>
						</div>
					</div>
					<div class="col-sm-12 col-md-6 no-space-left">
						<div  class="form">
							<form    method="post" id="contactFrm" name="contactFrm">
								<input type="text" required="" placeholder="First Name" value="" name="first" id="firstname" class="txt">
								<input type="text" required=""  placeholder="Last Name" value="" name="last" id="lastname" class="txt">
									<input type="text" id="email" required="" placeholder="Email" value="" name="email" class="txt" />
								<textarea placeholder="Message" name="mes" id="mess" type="text" class="txt_3"></textarea>
								<input type="button" value="submit" name="submit" id="submit">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="google-map">
		<iframe src="<?php if(isset($header[0])){echo $header[0]->header_location;}?>" allowfullscreen=""></iframe>
	</div>
</section>
<div class="call-to-action">
	<div class="container">
		<h3>IF YOU WANT TO BUSINESS WITH US </h3>

		<a id="#contactFrm" href="">CONTACT </a> </div>
</div>
