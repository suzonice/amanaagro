<style>
	.social_media li{ list-style:  none}
	</style>

<footer>
	<?php $headers=$this->db->get('headers')->row();?>

	<div class="container">

		<div class="col-sm-4">
			<div class="contactus">
				<h2>আমাদের সাথে যোগাযোগ করুন</h2>

				<ul class="list-ul">
					<li><i class="fa fa-map-marker"></i><?php echo $headers->header_two;?></li>
					<li><i class="fa fa-phone"></i>+88<?php echo $headers->header_mobile;?></li>
					<li><i class="fa fa-envelope"></i><a
							href="mailto:support@yourdomain.com"><?php echo $headers->header_email;?></a></li>
				</ul>
			</div>
		</div>

		<div class="col-sm-4">

			<h2>গুরুত্বপূর্ণ লিঙ্ক</h2>
			</p>
			<div class="form">

				<ul>
					<?php $links=$this->db->get('link')->result();
					foreach ($links as $link):
						?>

						<li style="list-style:none;margin-bottom:1px;border-bottom: 0px solid #ddd"><a style="display: block;color: black;padding:5px 5px;" href="<?php echo $link->link;?> " ><?php echo $link->link_title;?> </a> </li>
					<?php endforeach;?>

				</ul>
			</div>
		</div>

		<div class="col-sm-4">


			<h2>পরিদর্শক</h2>

			<ul class="social_media">
				<li>Today visitors: <?php echo $todayQueryData->today;?></li>
				<li>Last week visitors: <?php echo $lastWeekQueryData->lastWeek;?></li>
				<li>This month visitors: <?php echo $thisMonthQueryData->thisMonth;?></li>
				<li>Last month visitors: <?php echo $lastMonthQueryData->lastMonth;?></li>
				<li>Last year visitors: <?php echo $lastYearQueryData->year;?></li>
				<li>This year visitors: <?php echo $thisYearQueryData->thisyear;?></li>
				<li>Total visitors: <?php echo $totalQueryData->total;?></li>
			</ul>
		</div>
	</div>
</footer>
<!-- Footer Links End -->
<!-- Copy Rights Start -->
<div class="footer-wrapper">
	<div class="container">
		<p>&copy; Copyright 2017-
			<script type="text/javascript">
				var d = new Date();
				document.write(d.getFullYear());
			</script>
			Amana Agro Science | All Rights Reserved. developed by Shahinul islam suzon
		</p>
	</div>

</div>

</div>
<!-- Copy Rights End -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url()?>assets/fontend/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url()?>assets/fontend/js/jquery.animateNumber.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url()?>assets/fontend/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url()?>assets/fontend/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/fontend/js/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="<?php echo base_url()?>assets/fontend/js/custom.js"></script>
<script src="<?php echo base_url()?>assets/fontend/js/main.js"></script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83282272-3"></script>-->

</body>
</html>
<script>
	$(document).ready(function() {
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			loop: true,
			nav: true,
			margin: 10,
			autoplay:true,

			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 3
				},
				960: {
					items: 5
				},
				1200: {
					items: 6
				}
			}
		});
		owl.on('mousewheel', '.owl-stage', function(e) {
			if (e.deltaY > 0) {
				owl.trigger('next.owl');
			} else {
				owl.trigger('prev.owl');
			}
			e.preventDefault();
		});
		$('.carousel').carousel();

	});
</script>
<script>
</script>
<script>

	$(document).ready(function () {
		$("#messegeEmail").hide();
		$('#submit').click(function () {
			var first_name=$("#firstname").val();
			var lastname=$("#lastname").val();
			var mess=$("#mess").val();
			var email=$("#email").val();
			if(first_name.length ==""){
				alert("Enter Your Name");
			}
			if(mess.length ==""){
				alert("Enter Your Message");
			}
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			 if(!regex.test(email)){
			 	alert('Invalid Email Enter valid Email ');
			 }
			 else {
				 $.ajax({

					 type: "POST",
					 data: {first: first_name, last: lastname, mes: mess, email: email},
					 url: '<?php echo base_url()?>Home/Mail',
					 success: function (results) {
						 $("#messegeSent").html(results);
						 $("#messegeEmail").show().fadeOut(5000);

					 },
					 error: function (results) {


						 $("#messegeSent").html(results);
						 $("#messegeEmail").show().fadeOut(5000);
					 }
				 });
			 }
		});

	});
</script>
