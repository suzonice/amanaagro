 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li >
          <a href="<?php echo base_url();?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

        </li>
        <li class="treeview">
          <a href="<?php echo base_url();?>product-list">
            <i class="fa fa-user-secret"></i>
            <span>Category Option</span>
            <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
          </a>
          <ul class="treeview-menu">

              <li><a href="<?php echo base_url();?>category-create"><i class="fa fa-circle-o"></i> Add  category</a></li>
              <li><a href="<?php echo base_url();?>category-list"><i class="fa fa-circle-o"></i> View  category</a></li>
      </ul>
        </li>

		  <li class="treeview">
			  <a href="<?php echo base_url();?>product-list">
				  <i class="fa fa-user-secret"></i>
				  <span>Products Option</span>
				  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
			  </a>
			  <ul class="treeview-menu">
				  <li><a href="<?php echo base_url();?>product-create"><i class="fa fa-circle-o"></i> Add product</a></li>
				  <li><a href="<?php echo base_url();?>product-list"><i class="fa fa-circle-o"></i> View product</a></li>

			  </ul>
		  </li>
          <li class="treeview">
              <a href="<?php echo base_url();?>product-list">
                  <i class="fa fa-user-secret"></i>
                  <span>Member Option</span>
                  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="<?php echo base_url();?>member-create"><i class="fa fa-circle-o"></i> Add member</a></li>
                  <li><a href="<?php echo base_url();?>member-list"><i class="fa fa-circle-o"></i> View member</a></li>

              </ul>
          </li>

          <li class="treeview">
              <a href="<?php echo base_url();?>product-list">
                  <i class="fa fa-user-secret"></i>
                  <span>headers</span>
                  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="<?php echo base_url();?>header-create"><i class="fa fa-circle-o"></i> Add header</a></li>

              </ul>
          </li>

		  <li class="treeview">
			  <a href="<?php echo base_url();?>product-list">
				  <i class="fa fa-user-secret"></i>
				  <span>Statistic</span>
				  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
			  </a>
			  <ul class="treeview-menu">
				  <li><a href="<?php echo base_url();?>statistic-create"><i class="fa fa-circle-o"></i> Add Statistic</a></li>

			  </ul>
		  </li>
		  <li class="treeview">
			  <a href="<?php echo base_url();?>product-list">
				  <i class="fa fa-user-secret"></i>
				  <span>gelery options</span>
				  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
			  </a>
			  <ul class="treeview-menu">
				  <li><a href="<?php echo base_url();?>galery-create"><i class="fa fa-circle-o"></i> Add galery</a></li>
				  <li><a href="<?php echo base_url();?>galery-list"><i class="fa fa-circle-o"></i> View galery</a></li>


			  </ul>
		  </li>
		  <li class="treeview">
			  <a href="<?php echo base_url();?>product-list">
				  <i class="fa fa-user-secret"></i>
				  <span>Respons options</span>
				  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
			  </a>
			  <ul class="treeview-menu">
				  <li><a href="<?php echo base_url();?>respons-create"><i class="fa fa-circle-o"></i> add response</a></li>

				  <li><a href="<?php echo base_url();?>respons-list"><i class="fa fa-circle-o"></i> View response</a></li>


			  </ul>
		  </li>
		  <li class="treeview">
			  <a href="<?php echo base_url();?>product-list">
				  <i class="fa fa-user-secret"></i>
				  <span>Slider</span>
				  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
			  </a>
			  <ul class="treeview-menu">
				  <li><a href="<?php echo base_url();?>slider-create"><i class="fa fa-circle-o"></i> Add slider</a></li>
				  <li><a href="<?php echo base_url();?>slider-list"><i class="fa fa-circle-o"></i> View slider</a></li>

			  </ul>
		  </li>
		  <li class="treeview">
			  <a href="<?php echo base_url();?>product-list">
				  <i class="fa fa-user-secret"></i>
				  <span>Marketing Officers</span>
				  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
			  </a>
			  <ul class="treeview-menu">
				  <li><a href="<?php echo base_url();?>marketer-create"><i class="fa fa-circle-o"></i> Add Marketing Officer</a></li>
				  <li><a href="<?php echo base_url();?>marketer-list"><i class="fa fa-circle-o"></i> View Marketing Officers </a></li>

			  </ul>
		  </li>
		  <li class="treeview">
			  <a href="<?php echo base_url();?>product-list">
				  <i class="fa fa-user-secret"></i>
				  <span>link options</span>
				  <span class="pull-right-container">
			 <i class="fa fa-angle-left pull-right"></i>
			 </span>
			  </a>
			  <ul class="treeview-menu">
				  <li><a href="<?php echo base_url();?>link-create"><i class="fa fa-circle-o"></i> Add link</a></li>
				  <li><a href="<?php echo base_url();?>link-list"><i class="fa fa-circle-o"></i> View link</a></li>



			  </ul>
		  </li>




				  <li><a href="<?php echo base_url();?>setting-create"><i class="fa fa-circle-o"></i> Setting </a></li>

		  <li><a href="<?php echo base_url();?>user-create"><i class="fa fa-circle-o"></i> User </a></li>




	  </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
